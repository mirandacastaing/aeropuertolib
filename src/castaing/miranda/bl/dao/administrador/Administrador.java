package castaing.miranda.bl.dao.administrador;

import castaing.miranda.bl.Persona;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Administrador extends Persona {

    private LocalDate fechaNacimiento;
    private int edad;
    private char genero;

    public Administrador() {
        super();
        genero = ' ';
    }

    public Administrador(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, String fechaNacimiento, String genero) {
        super(nombre, apellidos, identificacion, correo, direccion, nacionalidad, contrasenna);
        this.fechaNacimiento = LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.edad = Period.between(this.fechaNacimiento, LocalDate.now()).getYears();
        this.genero = genero.charAt(0);
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "Administrador{" +
                super.toString() +
                "fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", genero=" + genero +
                '}';
    }

}
