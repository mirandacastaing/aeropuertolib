package castaing.miranda.bl.dao.administrador;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IAdministradorDao {

    void insertar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, String fechaNacimiento, String genero) throws java.sql.SQLException,
            Exception;

    ArrayList<Administrador> listar() throws java.sql.SQLException, Exception;

    void modificar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, String fechaNacimiento, String genero) throws java.sql.SQLException,
            Exception;

    void eliminar(String identificacion) throws java.sql.SQLException, Exception;

    Administrador buscarAdministradorPorIdentificacion(String identificacion) throws java.sql.SQLException, Exception;

    Administrador buscarAdministradorPorCorreo(String correo) throws java.sql.SQLException, Exception;

    Administrador validarCredenciales(String correo, String contrasenna) throws SQLException, Exception;

    boolean validarAdministrador(String correo, String identificacion) throws SQLException, Exception;

}
