package castaing.miranda.bl.dao.administrador;

import castaing.miranda.accesobd.Conector;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class SqlServerAdministradorDao implements IAdministradorDao {

    @Override
    public void insertar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, String fechaNacimiento, String genero) throws SQLException,
            Exception {
        String queryText;
        queryText = "INSERT INTO ADMINISTRADOR VALUES ('" + nombre + "', '" + apellidos + "', '" + identificacion +
                "', '" + correo + "', '" + direccion + "', '" + nacionalidad + "', '" + contrasenna + "', '" +
                Date.valueOf(LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) + "', '" +
                genero.charAt(0) + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String nombre, String apellidos, String identificacion, String correo, String direccion,
                          String nacionalidad, String contrasenna, String fechaNacimiento, String genero) throws
            SQLException, Exception {
        String queryText;
        queryText = "UPDATE ADMINISTRADOR SET NOMBRE = '" + nombre + "', APELLIDOS = '" + apellidos +
                "', IDENTIFICACION = '" + identificacion + "', CORREO = '" + correo + "', DIRECCION = '" + direccion +
                "', NACIONALIDAD = '" + nacionalidad + "', CONTRASENNA = '" + contrasenna + "', FECHA_NACIMIENTO = '" +
                Date.valueOf(LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) +
                "', GENERO = '" + genero.charAt(0) + "' WHERE IDENTIFICACION = '" + identificacion + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String identificacion) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM ADMINISTRADOR WHERE IDENTIFICACION = '" + identificacion + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<Administrador> listar() throws SQLException, Exception {
        ArrayList<Administrador> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, FECHA_NACIMIENTO, GENERO FROM ADMINISTRADOR");
        while (rs.next()) {
            Administrador a = new Administrador();
            a.setNombre(rs.getString("NOMBRE"));
            a.setApellidos(rs.getString("APELLIDOS"));
            a.setIdentificacion(rs.getString("IDENTIFICACION"));
            a.setCorreo(rs.getString("CORREO"));
            a.setDireccion(rs.getString("DIRECCION"));
            a.setNacionalidad(rs.getString("NACIONALIDAD"));
            a.setContrasenna(rs.getString("CONTRASENNA"));
            a.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO").toLocalDate());
            a.setGenero(rs.getString("GENERO").charAt(0));
            lista.add(a);
        }
        return lista;
    }

    @Override
    public Administrador buscarAdministradorPorIdentificacion(String identificacion) throws SQLException, Exception {
        System.out.println("17");
        Administrador a = new Administrador();
        System.out.println("18");
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, FECHA_NACIMIENTO, GENERO FROM ADMINISTRADOR WHERE " +
                "IDENTIFICACION = '" + identificacion + "'");
        System.out.println("19");
        while (rs.next()) {
            System.out.println("20");
            a.setNombre(rs.getString("NOMBRE"));
            a.setApellidos(rs.getString("APELLIDOS"));
            a.setIdentificacion(rs.getString("IDENTIFICACION"));
            a.setCorreo(rs.getString("CORREO"));
            a.setDireccion(rs.getString("DIRECCION"));
            a.setNacionalidad(rs.getString("NACIONALIDAD"));
            a.setContrasenna(rs.getString("CONTRASENNA"));
            a.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO").toLocalDate());
            a.setGenero(rs.getString("GENERO").charAt(0));
            System.out.println("21");
        }
        System.out.println("22");
        return a;
    }

    @Override
    public Administrador buscarAdministradorPorCorreo(String correo) throws SQLException, Exception {
        Administrador a = new Administrador();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, FECHA_NACIMIENTO, GENERO FROM ADMINISTRADOR WHERE CORREO = '" +
                correo + "'");
        while (rs.next()) {
            a.setNombre(rs.getString("NOMBRE"));
            a.setApellidos(rs.getString("APELLIDOS"));
            a.setIdentificacion(rs.getString("IDENTIFICACION"));
            a.setCorreo(rs.getString("CORREO"));
            a.setDireccion(rs.getString("DIRECCION"));
            a.setNacionalidad(rs.getString("NACIONALIDAD"));
            a.setContrasenna(rs.getString("CONTRASENNA"));
            a.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO").toLocalDate());
            a.setGenero(rs.getString("GENERO").charAt(0));
        }
        return a;
    }

    @Override
    public Administrador validarCredenciales(String correo, String contrasenna) throws SQLException, Exception {
        Administrador a = null;
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, FECHA_NACIMIENTO, GENERO FROM ADMINISTRADOR WHERE CORREO = '" +
                correo + "' AND CONTRASENNA = '" + contrasenna + "'");
        while (rs.next()) {
            a = new Administrador();
            a.setNombre(rs.getString("NOMBRE"));
            a.setApellidos(rs.getString("APELLIDOS"));
            a.setIdentificacion(rs.getString("IDENTIFICACION"));
            a.setCorreo(rs.getString("CORREO"));
            a.setDireccion(rs.getString("DIRECCION"));
            a.setNacionalidad(rs.getString("NACIONALIDAD"));
            a.setContrasenna(rs.getString("CONTRASENNA"));
            a.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO").toLocalDate());
            a.setGenero(rs.getString("GENERO").charAt(0));
        }
        return a;
    }

    @Override
    public boolean validarAdministrador(String correo, String identificacion) throws SQLException, Exception {
        boolean existe = false;
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, FECHA_NACIMIENTO, GENERO FROM ADMINISTRADOR WHERE CORREO = '" +
                correo + "' OR IDENTIFICACION = '" + identificacion + "'");
        while (rs.next()) {
            existe = true;
        }
        return existe;
    }

}
