package castaing.miranda.bl.dao.aerea.linea;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ILineaAereaDao {

    void insertar(String nombreComercial, String cedula, String nombreEmpresa, String pais, String logo) throws
            java.sql.SQLException, Exception;

    ArrayList<String[]> listar() throws java.sql.SQLException, Exception;

    void modificar(String nombreComercial, String cedula, String nombreEmpresa, String pais, String logo) throws
            java.sql.SQLException, Exception;

    void eliminar(String cedula) throws java.sql.SQLException, Exception;

    String[] buscarLineaAereaPorNombreComercial(String nombreComercial) throws SQLException, Exception;

    String[] buscarLineaAereaPorCedula(String cedula) throws SQLException, Exception;

}
