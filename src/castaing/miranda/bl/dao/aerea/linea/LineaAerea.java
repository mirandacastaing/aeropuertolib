package castaing.miranda.bl.dao.aerea.linea;

import castaing.miranda.bl.dao.pais.Pais;
import castaing.miranda.bl.dao.tripulacion.Tripulacion;
import castaing.miranda.bl.dao.vuelo.Vuelo;
import castaing.miranda.bl.dao.aeronave.Aeronave;

import java.util.ArrayList;
import java.util.Objects;

public class LineaAerea {

    private String nombreComercial;
    private String cedula;
    private String nombreEmpresa;
    private Pais tmpPais;
    private String logo;
    private ArrayList<Aeronave> aeronaves;
    private ArrayList<Tripulacion> tripulaciones;
    private ArrayList<Vuelo> vuelos;

    public LineaAerea() {
        nombreComercial = " ";
        cedula = " ";
        nombreEmpresa = " ";
        logo = " ";
    }

    public LineaAerea(String nombreComercial, String cedula, String nombreEmpresa, String logo) {
        this.nombreComercial = nombreComercial;
        this.cedula = cedula;
        this.nombreEmpresa = nombreEmpresa;
        this.tmpPais = new Pais();
        this.logo = logo;
        this.aeronaves = new ArrayList<>();
        this.tripulaciones = new ArrayList<>();
        this.vuelos = new ArrayList<>();
    }

    public LineaAerea(String nombreComercial, String cedula, String nombreEmpresa, Pais tmpPais, String logo) {
        this.nombreComercial = nombreComercial;
        this.cedula = cedula;
        this.nombreEmpresa = nombreEmpresa;
        this.tmpPais = tmpPais;
        this.logo = logo;
        this.aeronaves = new ArrayList<>();
        this.tripulaciones = new ArrayList<>();
        this.vuelos = new ArrayList<>();
    }

    public void agregarAeronave(Aeronave obj) {
        aeronaves.add(obj);
    }

    public void agregarTripulacion(Tripulacion obj) {
        tripulaciones.add(obj);
    }

    public void agregarVuelo(Vuelo obj) {
        vuelos.add(obj);
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public Pais getTmpPais() {
        return tmpPais;
    }

    public void setTmpPais(Pais tmpPais) {
        this.tmpPais = tmpPais;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ArrayList<Aeronave> getAeronaves() {
        return aeronaves;
    }

    public void setAeronaves(ArrayList<Aeronave> aeronaves) {
        this.aeronaves = aeronaves;
    }

    public ArrayList<Tripulacion> getTripulaciones() {
        return tripulaciones;
    }

    public void setTripulaciones(ArrayList<Tripulacion> tripulaciones) {
        this.tripulaciones = tripulaciones;
    }

    public ArrayList<Vuelo> getVuelos() {
        return vuelos;
    }

    public void setVuelos(ArrayList<Vuelo> vuelos) {
        this.vuelos = vuelos;
    }

    @Override
    public String toString() {
        return "LineaAerea{" +
                "nombreComercial='" + nombreComercial + '\'' +
                ", cedula='" + cedula + '\'' +
                ", nombreEmpresa='" + nombreEmpresa + '\'' +
                ", tmpPais=" + tmpPais +
                ", logo='" + logo + '\'' +
                ", aeronaves=" + aeronaves +
                ", tripulaciones=" + tripulaciones +
                ", vuelos=" + vuelos +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LineaAerea that = (LineaAerea) o;
        return Objects.equals(cedula, that.cedula);
    }

}
