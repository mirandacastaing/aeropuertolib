package castaing.miranda.bl.dao.aerea.linea;

import castaing.miranda.accesobd.Conector;
import castaing.miranda.bl.dao.factory.DaoFactory;
import castaing.miranda.bl.dao.pais.IPaisDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerLineaAereaDao implements ILineaAereaDao {

    @Override
    public void insertar(String nombreComercial, String cedula, String nombreEmpresa, String nombrePais, String logo)
            throws SQLException, Exception {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        String pais = daoPais.buscarPaisPorNombre(nombrePais).getCodigo();
        String queryText;
        queryText = "INSERT INTO LINEA_AEREA VALUES ('" + nombreComercial + "', '" + cedula + "', '" + nombreEmpresa +
                "', '" + pais + "', '" + logo + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String nombreComercial, String cedula, String nombreEmpresa, String nombrePais, String logo)
            throws SQLException, Exception {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        String pais = daoPais.buscarPaisPorNombre(nombrePais).getCodigo();
        String queryText;
        queryText = "UPDATE LINEA_AEREA SET NOMBRE_COMERCIAL = '" + nombreComercial + "', NOMBRE_EMPRESA = '" +
                nombreEmpresa + "', PAIS = '" + pais + "', LOGO = '" + logo + "' WHERE CEDULA = '" + cedula + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String cedula) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM TRIPULACION WHERE LINEA_AEREA = '" + cedula + "'";
        Conector.getConnector().ejecutarSql(queryText);
        queryText = "DELETE FROM LINEA_AEREA WHERE CEDULA = '" + cedula + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<String[]> listar() throws SQLException, Exception {
        ArrayList<String[]> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE_COMERCIAL, CEDULA, NOMBRE_EMPRESA, PAIS, " +
                "LOGO FROM LINEA_AEREA");
        while (rs.next()) {
            String[] l = new String[5];
            l[0] = rs.getString("NOMBRE_COMERCIAL");
            l[1] = rs.getString("CEDULA");
            l[2] = rs.getString("NOMBRE_EMPRESA");
            l[3] = rs.getString("PAIS");
            l[4] = rs.getString("LOGO");
            lista.add(l);
        }
        return lista;
    }

    @Override
    public String[] buscarLineaAereaPorNombreComercial(String nombreComercial) throws SQLException, Exception {
        String[] l = new String[5];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE_COMERCIAL, CEDULA, NOMBRE_EMPRESA, PAIS, " +
                "LOGO FROM LINEA_AEREA WHERE NOMBRE_COMERCIAL = '" + nombreComercial + "'");
        while (rs.next()) {
            l[1] = rs.getString("NOMBRE_COMERCIAL");
            l[0] = rs.getString("CEDULA");
            l[2] = rs.getString("NOMBRE_EMPRESA");
            l[3] = rs.getString("PAIS");
            l[4] = rs.getString("LOGO");
        }
        return l;
    }

    @Override
    public String[] buscarLineaAereaPorCedula(String cedula) throws SQLException, Exception {
        String[] l = new String[5];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE_COMERCIAL, CEDULA, NOMBRE_EMPRESA, PAIS, " +
                "LOGO FROM LINEA_AEREA WHERE CEDULA = '" + cedula + "'");
        while (rs.next()) {
            l[1] = rs.getString("NOMBRE_COMERCIAL");
            l[0] = rs.getString("CEDULA");
            l[2] = rs.getString("NOMBRE_EMPRESA");
            l[3] = rs.getString("PAIS");
            l[4] = rs.getString("LOGO");
        }
        return l;
    }

}
