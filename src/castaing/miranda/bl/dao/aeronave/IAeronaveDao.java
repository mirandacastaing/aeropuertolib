package castaing.miranda.bl.dao.aeronave;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IAeronaveDao {

    void insertar(String placa, String marca, String modelo, int capacidad, String lineaAerea) throws SQLException,
            Exception;

    ArrayList<String[]> listar() throws SQLException, Exception;

    void modificar(String placa, String marca, String modelo, int capacidad, String lineaAerea) throws SQLException,
            Exception;

    void eliminar(String placa) throws SQLException, Exception;

    String[] buscarAeronavePorPlaca(String placa) throws SQLException, Exception;

}
