package castaing.miranda.bl.dao.aeronave;

import castaing.miranda.accesobd.Conector;
import castaing.miranda.bl.dao.factory.DaoFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerAeronaveDao implements IAeronaveDao {

    @Override
    public void insertar(String placa, String marca, String modelo, int capacidad, String lineaAerea) throws
            SQLException, Exception {
        String queryText;
        queryText = "INSERT INTO AERONAVE VALUES ('" + placa + "', '" + marca + "', '" + modelo + "', " + capacidad +
                ", '" + lineaAerea + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String placa, String marca, String modelo, int capacidad, String lineaAerea) throws
            SQLException, Exception {
        String queryText;
        queryText = "UPDATE AERONAVE SET MARCA = '" + marca + "', MODELO = '" + modelo + "', CAPACIDAD = " + capacidad +
                ", LINEA_AEREA = '" + lineaAerea + "' WHERE PLACA = '" + placa + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String placa) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM AERONAVE WHERE PLACA = '" + placa + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<String[]> listar() throws SQLException, Exception {
        ArrayList<String[]> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT PLACA, MARCA, MODELO, CAPACIDAD, LINEA_AEREA " +
                "FROM AERONAVE");
        while (rs.next()) {
            String[] a = new String[5];
            a[0] = rs.getString("PLACA");
            a[1] = rs.getString("MARCA");
            a[2] = rs.getString("MODELO");
            a[3] = Integer.toString(rs.getInt("CAPACIDAD"));
            a[4] = rs.getString("LINEA_AEREA");
            lista.add(a);
        }
        return lista;
    }

    @Override
    public String[] buscarAeronavePorPlaca(String placa) throws SQLException, Exception {
        String[] a = new String[5];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT PLACA, MARCA, MODELO, CAPACIDAD, LINEA_AEREA FROM " +
                "AERONAVE WHERE PLACA = '" + placa + "'");
        while (rs.next()) {
            a[0] = rs.getString("PLACA");
            a[1] = rs.getString("MARCA");
            a[2] = rs.getString("MODELO");
            a[3] = rs.getString("CAPACIDAD");
            a[4] = rs.getString("LINEA_AEREA");
        }
        return a;
    }

}
