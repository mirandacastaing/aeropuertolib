package castaing.miranda.bl.dao.aeropuerto;

import castaing.miranda.bl.dao.pais.Pais;
import castaing.miranda.bl.dao.ubicacion.Ubicacion;
import castaing.miranda.bl.dao.administrador.Administrador;

import java.util.ArrayList;
import java.util.Objects;

public class Aeropuerto {

    private String nombre;
    private String codigo;
    private Pais tmpPais;
    private Administrador tmpAdministrador;
    private ArrayList<Ubicacion> ubicaciones;
    private double impuesto;

    public Aeropuerto() {
        nombre = " ";
        codigo = " ";
        this.ubicaciones = new ArrayList<>();
    }

    public Aeropuerto(String nombre, String codigo, Pais tmpPais, Administrador tmpAdministrador) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.tmpPais = tmpPais;
        this.tmpAdministrador = tmpAdministrador;
        this.ubicaciones = new ArrayList<>();
    }

    public void agregarUbicacion(Ubicacion obj) {
        ubicaciones.add(obj);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Pais getTmpPais() {
        return tmpPais;
    }

    public void setTmpPais(Pais tmpPais) {
        this.tmpPais = tmpPais;
    }

    public Administrador getTmpAdministrador() {
        return tmpAdministrador;
    }

    public void setTmpAdministrador(Administrador tmpAdministrador) {
        this.tmpAdministrador = tmpAdministrador;
    }

    public ArrayList<Ubicacion> getUbicaciones() {
        return ubicaciones;
    }

    public void setUbicaciones(ArrayList<Ubicacion> ubicaciones) {
        this.ubicaciones = ubicaciones;
    }

    public double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }

    @Override
    public String toString() {
        return "Aeropuerto{" +
                "nombre='" + nombre + '\'' +
                ", codigo='" + codigo + '\'' +
                ", tmpPais=" + tmpPais +
                ", tmpAdministrador=" + tmpAdministrador +
                ", ubicaciones=" + ubicaciones +
                ", impuesto=" + impuesto +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aeropuerto that = (Aeropuerto) o;
        return Objects.equals(codigo, that.codigo);
    }

}
