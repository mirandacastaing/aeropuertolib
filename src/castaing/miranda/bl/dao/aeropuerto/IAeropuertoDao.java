package castaing.miranda.bl.dao.aeropuerto;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IAeropuertoDao {

    void insertar(String nombre, String codigo, String pais, String administrador) throws java.sql.SQLException,
            Exception;

    ArrayList<String[]> listar() throws java.sql.SQLException, Exception;

    void modificar(String nombre, String codigo, String pais, String administrador) throws java.sql.SQLException,
            Exception;

    void eliminar(String codigo) throws java.sql.SQLException, Exception;

    boolean validarAeropuertoRegistrado() throws SQLException, Exception;

    boolean validarAdministradorAsignadoAAeropuerto(String correoAdministrador) throws SQLException, Exception;

    String[] getAeropuertoDeAdministrador(String correoAdministrador) throws SQLException, Exception;

    void modificarImpuesto(double impuesto, String codigo) throws java.sql.SQLException, Exception;

    Double getImpuesto(String codigo) throws java.sql.SQLException, Exception;

    String[] buscarAeropuertoPorNombre(String nombre) throws SQLException, Exception;

    String[] buscarAeropuertoPorCodigo(String codigo) throws SQLException, Exception;

}
