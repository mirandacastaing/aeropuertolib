package castaing.miranda.bl.dao.aeropuerto;

import castaing.miranda.accesobd.Conector;
import castaing.miranda.bl.dao.administrador.IAdministradorDao;
import castaing.miranda.bl.dao.factory.DaoFactory;
import castaing.miranda.bl.dao.pais.IPaisDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerAeropuertoDao implements IAeropuertoDao {

    @Override
    public void insertar(String nombre, String codigo, String pais, String administrador) throws SQLException, Exception {
        String queryText;
        queryText = "INSERT INTO AEROPUERTO (NOMBRE, CODIGO, PAIS, ADMINISTRADOR) VALUES ('" + nombre + "', '" + codigo
                + "', '" + pais + "', '" + administrador + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String nombre, String codigo, String pais, String administrador) throws SQLException,
            Exception {
        String queryText;
        queryText = "UPDATE AEROPUERTO SET NOMBRE = '" + nombre + "', PAIS = '" + pais + "', ADMINISTRADOR = '" +
                administrador + "' WHERE CODIGO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String codigo) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM UBICACION WHERE AEROPUERTO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
        queryText = "DELETE FROM AEROPUERTO WHERE CODIGO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<String[]> listar() throws SQLException, Exception {
        ArrayList<String[]> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, PAIS, ADMINISTRADOR FROM " +
                "AEROPUERTO");
        while (rs.next()) {
            String[] a = new String[4];
            a[0] = rs.getString("NOMBRE");
            a[1] = rs.getString("CODIGO");
            a[2] = rs.getString("PAIS");
            a[3] = rs.getString("ADMINISTRADOR");
            lista.add(a);
        }
        return lista;
    }

    @Override
    public boolean validarAeropuertoRegistrado() throws SQLException, Exception {
        boolean registrado = false;
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, PAIS, ADMINISTRADOR FROM " +
                "AEROPUERTO");
        while (rs.next()) {
            registrado = true;
        }
        return registrado;
    }

    @Override
    public boolean validarAdministradorAsignadoAAeropuerto(String correoAdministrador) throws SQLException, Exception
    {
        boolean asignado = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAdministradorDao daoAdministrador = factory.getAdministradorDao();
        String administrador = daoAdministrador.buscarAdministradorPorCorreo(correoAdministrador).getIdentificacion();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, PAIS, ADMINISTRADOR FROM " +
                "AEROPUERTO WHERE ADMINISTRADOR = '" + administrador + "'");
        while (rs.next()) {
            asignado = true;
        }
        return asignado;
    }

    @Override
    public String[] getAeropuertoDeAdministrador(String correoAdministrador) throws SQLException, Exception {
        String[] infoAeropuerto = new String[4];
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAdministradorDao daoAdministrador = factory.getAdministradorDao();
        String administrador = daoAdministrador.buscarAdministradorPorCorreo(correoAdministrador).getIdentificacion();
        System.out.println(administrador);
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, PAIS, ADMINISTRADOR FROM " +
                "AEROPUERTO WHERE ADMINISTRADOR = '" + administrador + "'");
        while (rs.next()) {
            infoAeropuerto[0] = rs.getString("NOMBRE");
            infoAeropuerto[1] = rs.getString("CODIGO");
            infoAeropuerto[2] = rs.getString("PAIS");
            infoAeropuerto[3] = rs.getString("ADMINISTRADOR");
        }
        return infoAeropuerto;
    }

    @Override
    public void modificarImpuesto(double impuesto, String codigo) throws SQLException, Exception {
        String queryText;
        queryText = "UPDATE AEROPUERTO SET IMPUESTO = " + impuesto + " WHERE CODIGO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public Double getImpuesto(String codigo) throws SQLException, Exception {
        String impuesto = "0";
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT IMPUESTO FROM AEROPUERTO WHERE CODIGO = '" + codigo
                + "'");
        while (rs.next()) {
            impuesto = rs.getString("IMPUESTO");
        }
        if (impuesto == null) {
            impuesto = "0";
        }
        return Double.parseDouble(impuesto);
    }

    @Override
    public String[] buscarAeropuertoPorNombre(String nombre) throws SQLException, Exception {
        String[] a = new String[4];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, PAIS, ADMINISTRADOR FROM " +
                "AEROPUERTO WHERE NOMBRE = '" + nombre + "'");
        while (rs.next()) {
            a[1] = rs.getString("NOMBRE");
            a[0] = rs.getString("CODIGO");
            a[2] = rs.getString("PAIS");
            a[3] = rs.getString("ADMINISTRADOR");
        }
        return a;
    }

    @Override
    public String[] buscarAeropuertoPorCodigo(String codigo) throws SQLException, Exception {
        String[] a = new String[5];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, PAIS, ADMINISTRADOR, IMPUESTO FROM " +
                "AEROPUERTO WHERE CODIGO = '" + codigo + "'");
        while (rs.next()) {
            a[1] = rs.getString("NOMBRE");
            a[0] = rs.getString("CODIGO");
            a[2] = rs.getString("PAIS");
            a[3] = rs.getString("ADMINISTRADOR");
            a[4] = Double.toString(rs.getDouble("IMPUESTO"));
        }
        return a;
    }

}
