package castaing.miranda.bl.dao.factory;

import castaing.miranda.bl.dao.administrador.IAdministradorDao;
import castaing.miranda.bl.dao.aerea.linea.ILineaAereaDao;
import castaing.miranda.bl.dao.aeronave.IAeronaveDao;
import castaing.miranda.bl.dao.aeropuerto.IAeropuertoDao;
import castaing.miranda.bl.dao.pais.IPaisDao;
import castaing.miranda.bl.dao.sala.ISalaDao;
import castaing.miranda.bl.dao.tripulacion.ITripulacionDao;
import castaing.miranda.bl.dao.tripulante.IMiembroTripulacionDao;
import castaing.miranda.bl.dao.ubicacion.IUbicacionDao;
import castaing.miranda.bl.dao.usuario.IUsuarioDao;
import castaing.miranda.bl.dao.vuelo.IVueloDao;

public abstract class DaoFactory {

    public static final int SQLSERVER = 1;
    public static final int MYSQL = 2;
    public static final int ORACLE = 3;

    public static DaoFactory getDaoFactory(int factory) {
        switch (factory) {
            case SQLSERVER:
                return new SqlServerDaoFactory();
            default:
                return null;
        }
    }

    public abstract IAdministradorDao getAdministradorDao();

    public abstract IPaisDao getPaisDao();

    public abstract IAeropuertoDao getAeropuertoDao();

    public abstract ILineaAereaDao getLineaAereaDao();

    public abstract ITripulacionDao getTripulacionDao();

    public abstract IMiembroTripulacionDao getMiembroTripulacionDao();

    public abstract IUsuarioDao getUsuarioDao();

    public abstract IUbicacionDao getUbicacionDao();

    public abstract ISalaDao getSalaDao();

    public abstract IAeronaveDao getAeronaveDao();

    public abstract IVueloDao getVueloDao();

}
