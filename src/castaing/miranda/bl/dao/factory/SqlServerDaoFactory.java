package castaing.miranda.bl.dao.factory;

import castaing.miranda.bl.dao.administrador.IAdministradorDao;
import castaing.miranda.bl.dao.administrador.SqlServerAdministradorDao;
import castaing.miranda.bl.dao.aerea.linea.ILineaAereaDao;
import castaing.miranda.bl.dao.aerea.linea.SqlServerLineaAereaDao;
import castaing.miranda.bl.dao.aeronave.IAeronaveDao;
import castaing.miranda.bl.dao.aeronave.SqlServerAeronaveDao;
import castaing.miranda.bl.dao.aeropuerto.IAeropuertoDao;
import castaing.miranda.bl.dao.aeropuerto.SqlServerAeropuertoDao;
import castaing.miranda.bl.dao.pais.IPaisDao;
import castaing.miranda.bl.dao.pais.SqlServerPaisDao;
import castaing.miranda.bl.dao.sala.ISalaDao;
import castaing.miranda.bl.dao.sala.SqlServerSalaDao;
import castaing.miranda.bl.dao.tripulacion.ITripulacionDao;
import castaing.miranda.bl.dao.tripulacion.SqlServerTripulacionDao;
import castaing.miranda.bl.dao.tripulante.IMiembroTripulacionDao;
import castaing.miranda.bl.dao.tripulante.SqlServerMiembroTripulacionDao;
import castaing.miranda.bl.dao.ubicacion.IUbicacionDao;
import castaing.miranda.bl.dao.ubicacion.SqlServerUbicacionDao;
import castaing.miranda.bl.dao.usuario.IUsuarioDao;
import castaing.miranda.bl.dao.usuario.SqlServerUsuarioDao;
import castaing.miranda.bl.dao.vuelo.IVueloDao;
import castaing.miranda.bl.dao.vuelo.SqlServerVueloDao;

public class SqlServerDaoFactory extends DaoFactory {

    @Override
    public IAdministradorDao getAdministradorDao() {
        return new SqlServerAdministradorDao();
    }

    @Override
    public IPaisDao getPaisDao() {
        return new SqlServerPaisDao();
    }

    @Override
    public IAeropuertoDao getAeropuertoDao() {
        return new SqlServerAeropuertoDao();
    }

    @Override
    public ILineaAereaDao getLineaAereaDao() {
        return new SqlServerLineaAereaDao();
    }

    @Override
    public ITripulacionDao getTripulacionDao() {
        return new SqlServerTripulacionDao();
    }

    @Override
    public IMiembroTripulacionDao getMiembroTripulacionDao() {
        return new SqlServerMiembroTripulacionDao();
    }

    @Override
    public IUsuarioDao getUsuarioDao() {
        return new SqlServerUsuarioDao();
    }

    @Override
    public IUbicacionDao getUbicacionDao() {
        return new SqlServerUbicacionDao();
    }

    @Override
    public ISalaDao getSalaDao() {
        return new SqlServerSalaDao();
    }

    @Override
    public IAeronaveDao getAeronaveDao() {
        return new SqlServerAeronaveDao();
    }

    @Override
    public IVueloDao getVueloDao() {
        return new SqlServerVueloDao();
    }

}
