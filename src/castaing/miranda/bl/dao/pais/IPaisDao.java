package castaing.miranda.bl.dao.pais;

import java.util.ArrayList;

public interface IPaisDao {

    void insertar(String codigo, String nombre, String abreviatura) throws java.sql.SQLException, Exception;

    ArrayList<Pais> listar() throws java.sql.SQLException, Exception;

    void modificar(String codigo, String nombre, String abreviatura) throws java.sql.SQLException, Exception;

    void eliminar(String codigo) throws java.sql.SQLException, Exception;

    Pais buscarPaisPorCodigo(String codigo) throws java.sql.SQLException, Exception;

    Pais buscarPaisPorNombre(String nombre) throws java.sql.SQLException, Exception;

}
