package castaing.miranda.bl.dao.pais;

import java.util.Objects;

public class Pais {

    private String codigo;
    private String nombre;
    private String abreviatura;

    public Pais() {
        codigo = " ";
        nombre = " ";
        abreviatura = " ";
    }

    public Pais(String codigo, String nombre, String abreviatura) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.abreviatura = abreviatura;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    @Override
    public String toString() {
        return "Pais{" +
                "codigo='" + codigo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", abreviatura='" + abreviatura + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pais pais = (Pais) o;
        return Objects.equals(codigo, pais.codigo);
    }

}
