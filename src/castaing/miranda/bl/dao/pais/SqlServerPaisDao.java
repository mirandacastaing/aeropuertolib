package castaing.miranda.bl.dao.pais;

import castaing.miranda.accesobd.Conector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerPaisDao implements IPaisDao {

    @Override
    public void insertar(String codigo, String nombre, String abreviatura) throws SQLException, Exception {
        String queryText;
        queryText = "INSERT INTO PAIS VALUES ('" + codigo + "', '" + nombre + "', '" + abreviatura + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String codigo, String nombre, String abreviatura) throws SQLException, Exception {
        String queryText;
        queryText = "UPDATE PAIS SET NOMBRE = '" + nombre + "', ABREVIATURA = '" + abreviatura +
                "' WHERE CODIGO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String codigo) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM PAIS WHERE CODIGO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<Pais> listar() throws SQLException, Exception {
        ArrayList<Pais> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT CODIGO, NOMBRE, ABREVIATURA FROM PAIS");
        while (rs.next()) {
            Pais p = new Pais();
            p.setCodigo(rs.getString("CODIGO"));
            p.setNombre(rs.getString("NOMBRE"));
            p.setAbreviatura(rs.getString("ABREVIATURA"));
            lista.add(p);
        }
        return lista;
    }

    @Override
    public Pais buscarPaisPorCodigo(String codigo) throws SQLException, Exception {
        Pais p = new Pais();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT CODIGO, NOMBRE, ABREVIATURA FROM PAIS WHERE " +
                "CODIGO = '" + codigo + "'");
        while (rs.next()) {
            p.setCodigo(rs.getString("CODIGO"));
            p.setNombre(rs.getString("NOMBRE"));
            p.setAbreviatura(rs.getString("ABREVIATURA"));
        }
        return p;
    }

    @Override
    public Pais buscarPaisPorNombre(String nombre) throws SQLException, Exception {
        Pais p = new Pais();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT CODIGO, NOMBRE, ABREVIATURA FROM PAIS WHERE " +
                "NOMBRE = '" + nombre + "'");
        while (rs.next()) {
            p.setCodigo(rs.getString("CODIGO"));
            p.setNombre(rs.getString("NOMBRE"));
            p.setAbreviatura(rs.getString("ABREVIATURA"));
        }
        return p;
    }

}
