package castaing.miranda.bl.dao.sala;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ISalaDao {

    void insertar(String nombre, String codigo, String administrador, char ubicacion) throws SQLException, Exception;

    ArrayList<String[]> listar(String aeropuerto) throws SQLException, Exception;

    void modificar(String nombre, String codigo, char ubicacion) throws SQLException, Exception;

    void eliminar(String codigo) throws SQLException, Exception;

    String[] buscarSalaPorCodigo(String codigo) throws SQLException, Exception;

    String[] buscarSalaPorNombre(String nombre) throws SQLException, Exception;

}
