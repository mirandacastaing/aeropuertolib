package castaing.miranda.bl.dao.sala;

import castaing.miranda.accesobd.Conector;
import castaing.miranda.bl.dao.administrador.Administrador;
import castaing.miranda.bl.dao.aeropuerto.IAeropuertoDao;
import castaing.miranda.bl.dao.factory.DaoFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerSalaDao implements ISalaDao {

    @Override
    public void insertar(String nombre, String codigo, String administrador, char ubicacion) throws SQLException,
            Exception {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoObject = factory.getAeropuertoDao();
        String aeropuerto = daoObject.getAeropuertoDeAdministrador(administrador)[1];
        String queryText;
        queryText = "INSERT INTO SALA VALUES ('" + nombre + "', '" + codigo + "', '" + aeropuerto + "', '" + ubicacion +
                "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String nombre, String codigo, char ubicacion) throws SQLException,
            Exception {
        String queryText;
        queryText = "UPDATE SALA SET NOMBRE = '" + nombre + "', UBICACION = '" + ubicacion + "' WHERE CODIGO = '" +
                codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String codigo) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM SALA WHERE CODIGO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<String[]> listar(String aeropuerto) throws SQLException, Exception {
        ArrayList<String[]> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, UBICACION FROM SALA");
        while (rs.next()) {
            String[] s = new String[3];
            s[0] = (rs.getString("NOMBRE"));
            s[1] = (rs.getString("CODIGO"));
            s[2] = (rs.getString("UBICACION"));
            lista.add(s);
        }
        return lista;
    }

    @Override
    public String[] buscarSalaPorCodigo(String codigo) throws SQLException, Exception {
        String[] sala = new String[3];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, UBICACION FROM SALA WHERE CODIGO = '" + codigo + "'");
        while (rs.next()) {
            sala[1] = rs.getString("NOMBRE");
            sala[0] = rs.getString("CODIGO");
            sala[2] = rs.getString("UBICACION");
        }
        return sala;
    }

    @Override
    public String[] buscarSalaPorNombre(String nombre) throws SQLException, Exception {
        String[] sala = new String[3];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, CODIGO, UBICACION FROM SALA WHERE " +
                "NOMBRE = '" + nombre + "'");
        while (rs.next()) {
            sala[1] = rs.getString("NOMBRE");
            sala[0] = rs.getString("CODIGO");
            sala[2] = rs.getString("UBICACION");
        }
        return sala;
    }

}
