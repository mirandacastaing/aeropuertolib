package castaing.miranda.bl.dao.tiquete;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Tiquete {

    private LocalDate fecha;
    private String numeroAsiento;
    private double precio;
    private char tipoAsiento;

    public Tiquete() {
        fecha = LocalDate.now();
        tipoAsiento = ' ';
    }

    public Tiquete(LocalDate fecha, String numeroAsiento, double precio, char tipoAsiento) {
        this.fecha = fecha;
        this.numeroAsiento = numeroAsiento;
        this.precio = precio;
        this.tipoAsiento = tipoAsiento;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getNumeroAsiento() {
        return numeroAsiento;
    }

    public void setNumeroAsiento(String numeroAsiento) {
        this.numeroAsiento = numeroAsiento;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public char getTipoAsiento() {
        return tipoAsiento;
    }

    public void setTipoAsiento(char tipoAsiento) {
        this.tipoAsiento = tipoAsiento;
    }

    @Override
    public String toString() {
        return "Tiquete{" +
                "fecha=" + fecha +
                ", numeroAsiento='" + numeroAsiento + '\'' +
                ", precio=" + precio +
                ", tipoAsiento=" + tipoAsiento +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tiquete tiquete = (Tiquete) o;
        return Objects.equals(numeroAsiento, tiquete.numeroAsiento);
    }

}
