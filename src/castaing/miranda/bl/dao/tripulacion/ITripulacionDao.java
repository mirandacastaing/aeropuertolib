package castaing.miranda.bl.dao.tripulacion;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ITripulacionDao {

    void insertar(String codigo, String nombre, String lineaAerea) throws java.sql.SQLException, Exception;

    ArrayList<String[]> listar() throws java.sql.SQLException, Exception;

    void modificar(String codigo, String nombre, String lineaAerea) throws java.sql.SQLException, Exception;

    void eliminar(String codigo) throws java.sql.SQLException, Exception;

    String[] buscarTripulacionPorNombre(String nombre, String lineaAerea) throws java.sql.SQLException, Exception;

    String[] buscarTripulacionPorCodigo(String codigo) throws SQLException, Exception;

}
