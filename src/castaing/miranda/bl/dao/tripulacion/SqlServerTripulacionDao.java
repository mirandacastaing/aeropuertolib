package castaing.miranda.bl.dao.tripulacion;

import castaing.miranda.accesobd.Conector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerTripulacionDao implements ITripulacionDao {

    @Override
    public void insertar(String codigo, String nombre, String lineaAerea) throws SQLException, Exception {
        String queryText;
        queryText = "INSERT INTO TRIPULACION VALUES ('" + codigo + "', '" + nombre + "', '" + lineaAerea + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String codigo, String nombre, String lineaAerea) throws SQLException, Exception {
        String queryText;
        queryText = "UPDATE TRIPULACION SET NOMBRE = '" + nombre + "', LINEA_AEREA = '" + lineaAerea +
                "' WHERE CODIGO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String codigo) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM TRIPULACION WHERE CODIGO = '" + codigo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<String[]> listar() throws SQLException, Exception {
        ArrayList<String[]> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT CODIGO, NOMBRE, LINEA_AEREA FROM TRIPULACION");
        while (rs.next()) {
            String[] t = new String[3];
            t[0] = rs.getString("CODIGO");
            t[1] = rs.getString("NOMBRE");
            t[2] = rs.getString("LINEA_AEREA");
            lista.add(t);
        }
        return lista;
    }

    @Override
    public String[] buscarTripulacionPorNombre(String nombre, String lineaAerea) throws SQLException, Exception {
        String[] t = new String[3];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT CODIGO, NOMBRE, LINEA_AEREA FROM TRIPULACION " +
                "WHERE NOMBRE = '" + nombre + "'");
        while (rs.next()) {
            t[0] = rs.getString("CODIGO");
            t[1] = rs.getString("NOMBRE");
            t[2] = rs.getString("LINEA_AEREA");
        }
        return t;
    }

    @Override
    public String[] buscarTripulacionPorCodigo(String codigo) throws SQLException, Exception {
        String[] t = new String[3];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT CODIGO, NOMBRE, LINEA_AEREA FROM TRIPULACION " +
                "WHERE CODIGO = '" + codigo + "'");
        while (rs.next()) {
            t[0] = rs.getString("CODIGO");
            t[1] = rs.getString("NOMBRE");
            t[2] = rs.getString("LINEA_AEREA");
        }
        return t;
    }

}
