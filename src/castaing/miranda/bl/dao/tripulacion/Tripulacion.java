package castaing.miranda.bl.dao.tripulacion;

import castaing.miranda.bl.dao.tripulante.MiembroTripulacion;

import java.util.ArrayList;
import java.util.Objects;

public class Tripulacion {

    private String codigo;
    private String nombre;
    private ArrayList<MiembroTripulacion> tmpMiembrosTripulacion;

    public Tripulacion() {
        codigo = " ";
        nombre = " ";
        tmpMiembrosTripulacion = new ArrayList<>();
    }

    public Tripulacion(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
        tmpMiembrosTripulacion = new ArrayList<>();
    }

    public Tripulacion(String codigo, String nombre, ArrayList<MiembroTripulacion> tmpMiembrosTripulacion) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.tmpMiembrosTripulacion = tmpMiembrosTripulacion;
        this.tmpMiembrosTripulacion = new ArrayList<>();
    }

    public void agregarMiembroTripulacion(MiembroTripulacion obj) {
        if (tmpMiembrosTripulacion == null) {
            tmpMiembrosTripulacion = new ArrayList<>();
        }
        tmpMiembrosTripulacion.add(obj);
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<MiembroTripulacion> getTmpMiembrosTripulacion() {
        return tmpMiembrosTripulacion;
    }

    public void setTmpMiembrosTripulacion(ArrayList<MiembroTripulacion> tmpMiembrosTripulacion) {
        this.tmpMiembrosTripulacion = tmpMiembrosTripulacion;
    }

    @Override
    public String toString() {
        return "Tripulacion{" +
                "codigo='" + codigo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", tmpMiembrosTripulacion=" + tmpMiembrosTripulacion +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tripulacion that = (Tripulacion) o;
        return Objects.equals(codigo, that.codigo);
    }

}
