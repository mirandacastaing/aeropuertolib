package castaing.miranda.bl.dao.tripulante;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IMiembroTripulacionDao {

    void insertar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, char genero, int annosExperiencia, String fechaGraduacion, String
            numeroLicencia, char puesto, String telefono, String nombreComercialLineaAerea, String nombreTripulacion)
            throws java.sql.SQLException, Exception;

    ArrayList<MiembroTripulacion> listar() throws java.sql.SQLException, Exception;

    void modificar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, char genero, int annosExperiencia, String fechaGraduacion, String
            numeroLicencia, char puesto, String telefono) throws java.sql.SQLException, Exception;

    void eliminar(String identificacion) throws java.sql.SQLException, Exception;

    MiembroTripulacion validarCredenciales(String correo, String contrasenna) throws SQLException, Exception;

    boolean validarMiembroTripulacion(String correo, String identificacion) throws SQLException, Exception;

}
