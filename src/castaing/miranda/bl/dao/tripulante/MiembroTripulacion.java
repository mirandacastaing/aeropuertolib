package castaing.miranda.bl.dao.tripulante;

import castaing.miranda.bl.Persona;
import castaing.miranda.bl.dao.vuelo.Vuelo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class MiembroTripulacion extends Persona {

    private char genero;
    private int annosExperiencia;
    private LocalDate fechaGraduacion;
    private String numeroLicencia;
    private char puesto;
    private String telefono;
    private ArrayList<Vuelo> vuelos;

    public MiembroTripulacion() {
        super();
        genero = ' ';
        puesto = ' ';
        telefono = " ";
        vuelos = new ArrayList<>();
    }

    public MiembroTripulacion(String nombre, String apellidos, String identificacion, String correo, String direccion,
                              String nacionalidad, String contrasenna, char genero, int annosExperiencia,
                              String fechaGraduacion, String numeroLicencia, char puesto, String telefono) {
        super(nombre, apellidos, identificacion, correo, direccion, nacionalidad, contrasenna);
        this.genero = genero;
        this.annosExperiencia = annosExperiencia;
        this.fechaGraduacion = LocalDate.parse(fechaGraduacion, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.numeroLicencia = numeroLicencia;
        this.puesto = puesto;
        this.telefono = telefono;
        vuelos = new ArrayList<>();
    }

    public void agregarVuelo(Vuelo obj) {
        if (vuelos == null) {
            vuelos = new ArrayList<>();
        }
        vuelos.add(obj);
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public int getAnnosExperiencia() {
        return annosExperiencia;
    }

    public void setAnnosExperiencia(int annosExperiencia) {
        this.annosExperiencia = annosExperiencia;
    }

    public LocalDate getFechaGraduacion() {
        return fechaGraduacion;
    }

    public void setFechaGraduacion(LocalDate fechaGraduacion) {
        this.fechaGraduacion = fechaGraduacion;
    }

    public String getNumeroLicencia() {
        return numeroLicencia;
    }

    public void setNumeroLicencia(String numeroLicencia) {
        this.numeroLicencia = numeroLicencia;
    }

    public char getPuesto() {
        return puesto;
    }

    public void setPuesto(char puesto) {
        this.puesto = puesto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public ArrayList<Vuelo> getVuelos() {
        return vuelos;
    }

    public void setVuelos(ArrayList<Vuelo> vuelos) {
        this.vuelos = vuelos;
    }

    @Override
    public String toString() {
        return "MiembroTripulacion{" +
                "genero=" + genero +
                ", annosExperiencia=" + annosExperiencia +
                ", fechaGraduacion=" + fechaGraduacion +
                ", numeroLicencia=" + numeroLicencia +
                ", puesto=" + puesto +
                ", telefono='" + telefono + '\'' +
                ", vuelos=" + vuelos +
                '}';
    }

}
