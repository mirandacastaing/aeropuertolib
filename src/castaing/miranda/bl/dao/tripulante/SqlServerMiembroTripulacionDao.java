package castaing.miranda.bl.dao.tripulante;

import castaing.miranda.accesobd.Conector;
import castaing.miranda.bl.dao.aerea.linea.ILineaAereaDao;
import castaing.miranda.bl.dao.factory.DaoFactory;
import castaing.miranda.bl.dao.tripulacion.ITripulacionDao;
import castaing.miranda.bl.dao.usuario.Usuario;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class SqlServerMiembroTripulacionDao implements IMiembroTripulacionDao {

    @Override
    public void insertar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, char genero, int annosExperiencia, String fechaGraduacion, String
            numeroLicencia, char puesto, String telefono, String nombreComercialLineaAerea, String nombreTripulacion)
            throws SQLException, Exception {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory.getLineaAereaDao();
        ITripulacionDao daoTripulacion = factory.getTripulacionDao();
        String lineaAerea = daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercialLineaAerea)[0];
        String codigoTripulacion = daoTripulacion.buscarTripulacionPorNombre(nombreTripulacion, lineaAerea)[0];
        String queryText;
        queryText = "INSERT INTO MIEMBRO_TRIPULACION VALUES ('" + nombre + "', '" + apellidos + "', '" + identificacion
                + "', '" + correo + "', '" + direccion + "', '" + nacionalidad + "', '" + contrasenna + "', '" + genero
                + "', " + annosExperiencia + ", '" + Date.valueOf(LocalDate.parse(fechaGraduacion,
                DateTimeFormatter.ofPattern("yyyy-MM-dd"))) + "', '" + numeroLicencia + "', '" + puesto + "', '" +
                telefono + "', '" + codigoTripulacion + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String nombre, String apellidos, String identificacion, String correo, String direccion,
                          String nacionalidad, String contrasenna, char genero, int annosExperiencia, String
                                      fechaGraduacion, String numeroLicencia, char puesto, String telefono) throws
            SQLException, Exception {
        String queryText;
        queryText = "UPDATE MIEMBRO_TRIPULACION SET NOMBRE = '" + nombre + "', APELLIDOS = '" + apellidos +
                "', IDENTIFICACION = '" + identificacion + "', CORREO = '" + correo + "', DIRECCION = '" + direccion +
                "', NACIONALIDAD = '" + nacionalidad + "', CONTRASENNA = '" + contrasenna + "', GENERO = '" + genero +
                "', ANNOS_EXPERIENCIA = " + annosExperiencia + ", FECHA_GRADUACION = '" +
                Date.valueOf(LocalDate.parse(fechaGraduacion,DateTimeFormatter.ofPattern("yyyy-MM-dd"))) +
                "', NUMERO_LICENCIA = '" + numeroLicencia + "', PUESTO = '" + puesto + "', TELEFONO = '" + telefono +
                "' WHERE IDENTIFICACION = '" + identificacion + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String identificacion) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM MIEMBRO_TRIPULACION WHERE IDENTIFICACION = '" + identificacion + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<MiembroTripulacion> listar() throws SQLException, Exception {
        ArrayList<MiembroTripulacion> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, GENERO, ANNOS_EXPERIENCIA, FECHA_GRADUACION, NUMERO_LICENCIA, " +
                "PUESTO, TELEFONO FROM MIEMBRO_TRIPULACION");
        while (rs.next()) {
            MiembroTripulacion m = new MiembroTripulacion();
            m.setNombre(rs.getString("NOMBRE"));
            m.setApellidos(rs.getString("APELLIDOS"));
            m.setIdentificacion(rs.getString("IDENTIFICACION"));
            m.setCorreo(rs.getString("CORREO"));
            m.setDireccion(rs.getString("DIRECCION"));
            m.setNacionalidad(rs.getString("NACIONALIDAD"));
            m.setContrasenna(rs.getString("CONTRASENNA"));
            m.setGenero(rs.getString("GENERO").charAt(0));
            m.setAnnosExperiencia(rs.getInt("ANNOS_EXPERIENCIA"));
            m.setFechaGraduacion(rs.getDate("FECHA_GRADUACION").toLocalDate());
            m.setNumeroLicencia(rs.getString("NUMERO_LICENCIA"));
            m.setPuesto(rs.getString("PUESTO").charAt(0));
            m.setTelefono(rs.getString("TELEFONO"));
            lista.add(m);
        }
        return lista;
    }

    @Override
    public MiembroTripulacion validarCredenciales(String correo, String contrasenna) throws SQLException, Exception {
        MiembroTripulacion m = null;
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, GENERO, ANNOS_EXPERIENCIA, FECHA_GRADUACION, NUMERO_LICENCIA, " +
                "PUESTO, TELEFONO FROM MIEMBRO_TRIPULACION WHERE CORREO = '" + correo + "' AND CONTRASENNA = '" +
                contrasenna + "'");
        while (rs.next()) {
            m = new MiembroTripulacion();
            m.setNombre(rs.getString("NOMBRE"));
            m.setApellidos(rs.getString("APELLIDOS"));
            m.setIdentificacion(rs.getString("IDENTIFICACION"));
            m.setCorreo(rs.getString("CORREO"));
            m.setDireccion(rs.getString("DIRECCION"));
            m.setNacionalidad(rs.getString("NACIONALIDAD"));
            m.setContrasenna(rs.getString("CONTRASENNA"));
            m.setGenero(rs.getString("GENERO").charAt(0));
            m.setAnnosExperiencia(rs.getInt("ANNOS_EXPERIENCIA"));
            m.setFechaGraduacion(rs.getDate("FECHA_GRADUACION").toLocalDate());
            m.setNumeroLicencia(rs.getString("NUMERO_LICENCIA"));
            m.setPuesto(rs.getString("PUESTO").charAt(0));
            m.setTelefono(rs.getString("TELEFONO"));
        }
        return m;
    }

    @Override
    public boolean validarMiembroTripulacion(String correo, String identificacion) throws SQLException, Exception {
        boolean existe = false;
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, GENERO, ANNOS_EXPERIENCIA, FECHA_GRADUACION, NUMERO_LICENCIA, " +
                "PUESTO, TELEFONO FROM MIEMBRO_TRIPULACION WHERE CORREO = '" + correo + "' OR IDENTIFICACION = '" +
                identificacion + "'");
        while (rs.next()) {
            existe = true;
        }
        return existe;
    }

}
