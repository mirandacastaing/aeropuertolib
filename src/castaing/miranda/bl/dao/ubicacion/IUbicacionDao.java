package castaing.miranda.bl.dao.ubicacion;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IUbicacionDao {

    void insertar(char planta, String descripcion, String administrador) throws SQLException, Exception;

    ArrayList<Ubicacion> listar(String aeropuerto) throws SQLException, Exception;

    void modificar(char planta, String descripcion, String administrador) throws SQLException,
            Exception;

    void eliminar(char planta, String administrador) throws SQLException, Exception;

    Ubicacion buscarUbicacionPorPlanta(char planta) throws SQLException, Exception;

    Ubicacion buscarUbicacionPorPlantaAeropuerto(char planta, String aeropuerto) throws SQLException, Exception;

}
