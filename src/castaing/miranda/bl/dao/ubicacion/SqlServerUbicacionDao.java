package castaing.miranda.bl.dao.ubicacion;

import castaing.miranda.accesobd.Conector;
import castaing.miranda.bl.dao.administrador.Administrador;
import castaing.miranda.bl.dao.aeropuerto.IAeropuertoDao;
import castaing.miranda.bl.dao.factory.DaoFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerUbicacionDao implements IUbicacionDao {

    @Override
    public void insertar(char planta, String descripcion, String administrador) throws SQLException, Exception {
        String queryText;
        queryText = "INSERT INTO UBICACION VALUES ('" + planta + "', '" + descripcion + "', '" + administrador + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(char planta, String descripcion, String administrador) throws SQLException, Exception {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoObject = factory.getAeropuertoDao();
        String aeropuerto = daoObject.getAeropuertoDeAdministrador(administrador)[1];
        System.out.println(planta);
        System.out.println(descripcion);
        System.out.println(administrador);
        System.out.println(aeropuerto);
        String queryText;
        queryText = "UPDATE UBICACION SET DESCRIPCION = '" + descripcion + "' WHERE PLANTA = '" + planta + "' AND " +
                "AEROPUERTO = '" + aeropuerto + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(char planta, String administrador) throws SQLException, Exception {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoObject = factory.getAeropuertoDao();
        String aeropuerto = daoObject.getAeropuertoDeAdministrador(administrador)[1];
        String queryText;
        queryText = "DELETE FROM SALA WHERE UBICACION = '" + planta + "' AND AEROPUERTO = '" + aeropuerto + "'";
        Conector.getConnector().ejecutarSql(queryText);
        queryText = "DELETE FROM UBICACION WHERE PLANTA = '" + planta + "' AND AEROPUERTO = '" + aeropuerto + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<Ubicacion> listar(String aeropuerto) throws SQLException, Exception {
        ArrayList<Ubicacion> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT PLANTA, DESCRIPCION FROM UBICACION WHERE " +
                "AEROPUERTO = '" + aeropuerto + "'");
        while (rs.next()) {
            Ubicacion u = new Ubicacion();
            u.setPlanta(rs.getString("PLANTA").charAt(0));
            u.setDescripcion(rs.getString("DESCRIPCION"));
            lista.add(u);
        }
        return lista;
    }

    @Override
    public Ubicacion buscarUbicacionPorPlanta(char planta) throws SQLException, Exception {
        Ubicacion u = new Ubicacion();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT PLANTA, DESCRIPCION FROM UBICACION WHERE PLANTA " +
                "= '" + planta +"'");
        while (rs.next()) {
            u.setPlanta(rs.getString("PLANTA").charAt(0));
            u.setDescripcion(rs.getString("DESCRIPCION"));
        }
        return u;
    }

    @Override
    public Ubicacion buscarUbicacionPorPlantaAeropuerto(char planta, String aeropuerto) throws SQLException, Exception {
        Ubicacion u = new Ubicacion();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT PLANTA, DESCRIPCION FROM UBICACION WHERE PLANTA " +
                "= '" + planta +"' AND AEROPUERTO = '" + aeropuerto + "'");
        while (rs.next()) {
            u.setPlanta(rs.getString("PLANTA").charAt(0));
            u.setDescripcion(rs.getString("DESCRIPCION"));
        }
        return u;
    }

}
