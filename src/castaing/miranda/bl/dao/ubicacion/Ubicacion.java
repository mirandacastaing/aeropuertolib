package castaing.miranda.bl.dao.ubicacion;

import castaing.miranda.bl.dao.sala.Sala;

import java.util.ArrayList;

public class Ubicacion {

    private char planta;
    private String descripcion;
    private ArrayList<Sala> salas;

    public Ubicacion() {
        planta = ' ';
        descripcion = " ";
        this.salas = new ArrayList<>();
    }

    public Ubicacion(char planta, String descripcion) {
        this.planta = planta;
        this.descripcion = descripcion;
        this.salas = new ArrayList<>();
    }

    public void agregarSala(Sala obj) {
        salas.add(obj);
    }

    public char getPlanta() {
        return planta;
    }

    public void setPlanta(char planta) {
        this.planta = planta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<Sala> getSalas() {
        return salas;
    }

    public void setSalas(ArrayList<Sala> salas) {
        this.salas = salas;
    }

    @Override
    public String toString() {
        return "Ubicacion{" +
                "planta=" + planta +
                ", descripcion='" + descripcion + '\'' +
                ", salas=" + salas +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ubicacion ubicacion = (Ubicacion) o;
        return planta == ubicacion.planta;
    }

}
