package castaing.miranda.bl.dao.usuario;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IUsuarioDao {

    void insertar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, String provincia, String canton, String distrito, String fechaNacimiento)
            throws java.sql.SQLException, Exception;

    ArrayList<Usuario> listar() throws java.sql.SQLException, Exception;

    void modificar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, String provincia, String canton, String distrito, String fechaNacimiento)
            throws java.sql.SQLException, Exception;

    void eliminar(String identificacion) throws java.sql.SQLException, Exception;

    String[] validarCredenciales(String correo, String contrasenna) throws SQLException, Exception;

    boolean validarUsuario(String correo, String identificacion) throws SQLException, Exception;

}
