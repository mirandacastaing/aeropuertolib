package castaing.miranda.bl.dao.usuario;

import castaing.miranda.accesobd.Conector;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class SqlServerUsuarioDao implements IUsuarioDao {

    @Override
    public void insertar(String nombre, String apellidos, String identificacion, String correo, String direccion, String
            nacionalidad, String contrasenna, String provincia, String canton, String distrito, String fechaNacimiento)
            throws SQLException, Exception {
        String queryText;
        queryText = "INSERT INTO USUARIO VALUES ('" + nombre + "', '" + apellidos + "', '" + identificacion + "', '" +
                correo + "', '" + direccion + "', '" + nacionalidad + "', '" + contrasenna + "', '" + provincia + "', '"
                + canton + "', '" + distrito + "', '" +
                Date.valueOf(LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String nombre, String apellidos, String identificacion, String correo, String direccion,
                          String nacionalidad, String contrasenna, String provincia, String canton, String distrito,
                          String fechaNacimiento) throws SQLException, Exception {
        String queryText;
        queryText = "UPDATE USUARIO SET NOMBRE = '" + nombre + "', APELLIDOS = '" + apellidos +
                "', IDENTIFICACION = '" + identificacion + "', CORREO = '" + correo + "', DIRECCION = '" + direccion +
                "', NACIONALIDAD = '" + nacionalidad + "', CONTRASENNA = '" + contrasenna + "', PROVINCIA = '" +
                provincia + "', CANTON = '" + canton + "', DISTRITO = '" + distrito + "', FECHA_NACIMIENTO = '" +
                Date.valueOf(LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) +
                "' WHERE IDENTIFICACION = '" + identificacion + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String identificacion) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM USUARIO WHERE IDENTIFICACION = '" + identificacion + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<Usuario> listar() throws SQLException, Exception {
        ArrayList<Usuario> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, PROVINCIA, CANTON, DISTRITO, FECHA_NACIMIENTO FROM USUARIO");
        while (rs.next()) {
            Usuario u = new Usuario();
            u.setNombre(rs.getString("NOMBRE"));
            u.setApellidos(rs.getString("APELLIDOS"));
            u.setIdentificacion(rs.getString("IDENTIFICACION"));
            u.setCorreo(rs.getString("CORREO"));
            u.setDireccion(rs.getString("DIRECCION"));
            u.setNacionalidad(rs.getString("NACIONALIDAD"));
            u.setContrasenna(rs.getString("CONTRASENNA"));
            u.setProvincia(rs.getString("PROVINCIA"));
            u.setCanton(rs.getString("CANTON"));
            u.setDistrito(rs.getString("DISTRITO"));
            u.setFechaNacimiento(rs.getString("FECHA_NACIMIENTO"));
            lista.add(u);
        }
        return lista;
    }

    @Override
    public String[] validarCredenciales(String correo, String contrasenna) throws SQLException, Exception {
        String[] u = null;
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, PROVINCIA, CANTON, DISTRITO, FECHA_NACIMIENTO FROM USUARIO " +
                "WHERE CORREO = '" + correo + "' AND CONTRASENNA = '" + contrasenna + "'");
        while (rs.next()) {
            u = new String[11];
            u[0] = rs.getString("NOMBRE");
            u[1] = rs.getString("APELLIDOS");
            u[2] = rs.getString("IDENTIFICACION");
            u[3] = rs.getString("CORREO");
            u[4] = rs.getString("DIRECCION");
            u[5] = rs.getString("NACIONALIDAD");
            u[6] = rs.getString("CONTRASENNA");
            u[7] = rs.getString("PROVINCIA");
            u[8] = rs.getString("CANTON");
            u[9] = rs.getString("DISTRITO");
            u[10] = rs.getString("FECHA_NACIMIENTO");
            System.out.println("Hola2");
        }
        System.out.println("Hola");
        return u;
    }

    @Override
    public boolean validarUsuario(String correo, String identificacion) throws SQLException, Exception {
        boolean existe = false;
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT NOMBRE, APELLIDOS, IDENTIFICACION, CORREO, " +
                "DIRECCION, NACIONALIDAD, CONTRASENNA, PROVINCIA, CANTON, DISTRITO, FECHA_NACIMIENTO FROM USUARIO " +
                "WHERE CORREO = '" + correo + "' OR IDENTIFICACION = '" + identificacion + "'");
        while (rs.next()) {
            existe = true;
        }
        return existe;
    }

}
