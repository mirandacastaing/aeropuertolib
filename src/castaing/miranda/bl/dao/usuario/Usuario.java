package castaing.miranda.bl.dao.usuario;

import castaing.miranda.bl.Persona;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Usuario extends Persona {

    private String provincia;
    private String canton;
    private String distrito;
    private LocalDate fechaNacimiento;
    private int edad;

    public Usuario() {
        super();
        provincia = " ";
        canton = " ";
        distrito = " ";
    }

    public Usuario(String nombre, String apellidos, String identificacion, String correo, String direccion,
                   String nacionalidad, String contrasenna, String provincia, String canton, String distrito,
                   String fechaNacimiento) {
        super(nombre, apellidos, identificacion, correo, direccion, nacionalidad, contrasenna);
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.fechaNacimiento = LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.edad = Period.between(this.fechaNacimiento, LocalDate.now()).getYears();
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("d/M/yyyy"));
    }

    public int getEdad() {
        return edad;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                super.toString() +
                "provincia='" + provincia + '\'' +
                ", canton='" + canton + '\'' +
                ", distrito='" + distrito + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                '}';
    }

}
