package castaing.miranda.bl.dao.vuelo;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IVueloDao {

    void insertar(String aeropuertoOrigen, String horaLlegada, String horaSalida, char estado, String aeropuertoDestino,
                  char tipo, String aeronave, String sala, String tripulacion, String numeroVuelo,
                  int asientosDisponibles, String fechaLlegada, String fechaSalida, double precio, String lineaAerea)
            throws SQLException, Exception;

    ArrayList<String[]> listar() throws SQLException, Exception;

    ArrayList<String[]> listarDeLlegada() throws SQLException, Exception;

    ArrayList<String[]> listarDeSalida() throws SQLException, Exception;

    void modificar(String aeropuertoOrigen, String horaLlegada, String horaSalida, char estado,
                   String aeropuertoDestino, char tipo, String aeronave, String sala, String tripulacion,
                   String numeroVuelo, String fechaLlegada, String fechaSalida, double precio, String lineaAerea)
            throws SQLException, Exception;

    void eliminar(String numero) throws SQLException, Exception;

    String[] buscarVueloPorNumero(String numero) throws SQLException, Exception;

    void restarAsiento(String numeroVuelo) throws SQLException, Exception;

}
