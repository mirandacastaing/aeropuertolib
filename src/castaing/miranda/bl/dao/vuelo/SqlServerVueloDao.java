package castaing.miranda.bl.dao.vuelo;

import castaing.miranda.accesobd.Conector;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class SqlServerVueloDao implements IVueloDao {

    @Override
    public void insertar(String aeropuertoOrigen, String horaLlegada, String horaSalida, char estado,
                         String aeropuertoDestino, char tipo, String aeronave, String sala, String tripulacion,
                         String numeroVuelo, int asientosDisponibles, String fechaLlegada, String fechaSalida,
                         double precio, String lineaAerea) throws SQLException, Exception {
        String queryText;
        queryText = "INSERT INTO VUELO VALUES ('" + aeropuertoOrigen + "', '" + horaLlegada + "', '" + horaSalida +
                "', '" + estado + "', '" + aeropuertoDestino + "', '" + tipo + "', '" + aeronave + "', '" + sala +
                "', '" + tripulacion + "', '" + numeroVuelo + "', " + asientosDisponibles + ", '" +
                Date.valueOf(LocalDate.parse(fechaLlegada, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) + "', '" +
                Date.valueOf(LocalDate.parse(fechaSalida, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) + "', " + precio +
                ", '" + lineaAerea + "')";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void modificar(String aeropuertoOrigen, String horaLlegada, String horaSalida, char estado,
                          String aeropuertoDestino, char tipo, String aeronave, String sala, String tripulacion,
                          String numeroVuelo, String fechaLlegada, String fechaSalida, double precio, String lineaAerea)
            throws SQLException, Exception {
        String queryText;
        queryText = "UPDATE VUELO SET AEROPUERTO_ORIGEN = '" + aeropuertoOrigen + "', HORA_LLEGADA = '" + horaLlegada +
                "', HORA_SALIDA = '" + horaSalida + "', ESTADO = '" + estado + "', AEROPUERTO_DESTINO = '" +
                aeropuertoDestino + "', TIPO = '" + tipo + "', AERONAVE = '" + aeronave + "', SALA = '" + sala + "', " +
                "TRIPULACION = '" + tripulacion + "', FECHA_LLEGADA = '" +
                Date.valueOf(LocalDate.parse(fechaLlegada, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) + "', " +
                "FECHA_SALIDA = '" +
                Date.valueOf(LocalDate.parse(fechaSalida, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) + "', PRECIO = " +
                precio + ", LINEA_AEREA = '" + lineaAerea + "' WHERE NUMERO_VUELO = '" + numeroVuelo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void restarAsiento(String numeroVuelo) throws SQLException, Exception {
        String queryText;
        queryText = "UPDATE VUELO SET ASIENTOS_DISPONIBLES = ASIENTOS_DISPONIBLES - 1 WHERE NUMERO_VUELO = '"
                + numeroVuelo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public void eliminar(String numeroVuelo) throws SQLException, Exception {
        String queryText;
        queryText = "DELETE FROM VUELO WHERE NUMERO_VUELO = '" + numeroVuelo + "'";
        Conector.getConnector().ejecutarSql(queryText);
    }

    @Override
    public ArrayList<String[]> listar() throws SQLException, Exception {
        ArrayList<String[]> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT AEROPUERTO_ORIGEN, HORA_LLEGADA, HORA_SALIDA, " +
                "ESTADO, AEROPUERTO_DESTINO, TIPO, AERONAVE, SALA, TRIPULACION, NUMERO_VUELO, ASIENTOS_DISPONIBLES, " +
                "FECHA_LLEGADA, FECHA_SALIDA, PRECIO, LINEA_AEREA FROM VUELO");
        while (rs.next()) {
            String[] v = new String[15];
            v[0] = rs.getString("NUMERO_VUELO");
            v[1] = rs.getString("AEROPUERTO_ORIGEN");
            v[2] = rs.getString("SALA");
            v[3] = rs.getString("HORA_SALIDA");
            v[4] = rs.getString("AEROPUERTO_DESTINO");
            v[5] = rs.getString("HORA_LLEGADA");
            v[6] = rs.getString("ESTADO");
            v[7] = rs.getString("TIPO");
            v[8] = rs.getString("AERONAVE");
            v[9] = rs.getString("TRIPULACION");
            v[10] = rs.getString("ASIENTOS_DISPONIBLES");
            v[11] = rs.getString("LINEA_AEREA");
            v[12] = rs.getString("FECHA_LLEGADA");
            v[13] = rs.getString("FECHA_SALIDA");
            v[14] = rs.getString("PRECIO");
            lista.add(v);
        }
        return lista;
    }

    @Override
    public ArrayList<String[]> listarDeLlegada() throws SQLException, Exception {
        ArrayList<String[]> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT AEROPUERTO_ORIGEN, HORA_LLEGADA, HORA_SALIDA, " +
                "ESTADO, AEROPUERTO_DESTINO, TIPO, AERONAVE, SALA, TRIPULACION, NUMERO_VUELO, ASIENTOS_DISPONIBLES, " +
                "FECHA_LLEGADA, FECHA_SALIDA, PRECIO, LINEA_AEREA FROM VUELO WHERE TIPO = 'L'");
        while (rs.next()) {
            String[] v = new String[14];
            v[0] = rs.getString("NUMERO_VUELO");
            v[1] = rs.getString("AEROPUERTO_ORIGEN");
            v[2] = rs.getString("SALA");
            v[3] = rs.getString("HORA_SALIDA");
            v[4] = rs.getString("AEROPUERTO_DESTINO");
            v[5] = rs.getString("HORA_LLEGADA");
            v[6] = rs.getString("ESTADO");
            v[7] = rs.getString("TIPO");
            v[8] = rs.getString("AERONAVE");
            v[9] = rs.getString("TRIPULACION");
            v[10] = rs.getString("ASIENTOS_DISPONIBLES");
            v[11] = rs.getString("LINEA_AEREA");
            v[12] = rs.getString("FECHA_LLEGADA");
            v[13] = rs.getString("FECHA_SALIDA");
            lista.add(v);
        }
        return lista;
    }

    @Override
    public ArrayList<String[]> listarDeSalida() throws SQLException, Exception {
        ArrayList<String[]> lista = new ArrayList<>();
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT AEROPUERTO_ORIGEN, HORA_LLEGADA, HORA_SALIDA, " +
                "ESTADO, AEROPUERTO_DESTINO, TIPO, AERONAVE, SALA, TRIPULACION, NUMERO_VUELO, ASIENTOS_DISPONIBLES, " +
                "FECHA_LLEGADA, FECHA_SALIDA, PRECIO, LINEA_AEREA FROM VUELO WHERE TIPO = 'S'");
        while (rs.next()) {
            String[] v = new String[14];
            v[0] = rs.getString("NUMERO_VUELO");
            v[1] = rs.getString("AEROPUERTO_ORIGEN");
            v[2] = rs.getString("SALA");
            v[3] = rs.getString("HORA_SALIDA");
            v[4] = rs.getString("AEROPUERTO_DESTINO");
            v[5] = rs.getString("HORA_LLEGADA");
            v[6] = rs.getString("ESTADO");
            v[7] = rs.getString("TIPO");
            v[8] = rs.getString("AERONAVE");
            v[9] = rs.getString("TRIPULACION");
            v[10] = rs.getString("ASIENTOS_DISPONIBLES");
            v[11] = rs.getString("LINEA_AEREA");
            v[12] = rs.getString("FECHA_LLEGADA");
            v[13] = rs.getString("FECHA_SALIDA");
            lista.add(v);
        }
        return lista;
    }

    @Override
    public String[] buscarVueloPorNumero(String numero) throws SQLException, Exception {
        String[] v = new String[15];
        ResultSet rs = Conector.getConnector().ejecutarQuery("SELECT AEROPUERTO_ORIGEN, HORA_LLEGADA, HORA_SALIDA, " +
                "ESTADO, AEROPUERTO_DESTINO, TIPO, AERONAVE, SALA, TRIPULACION, NUMERO_VUELO, ASIENTOS_DISPONIBLES, " +
                "FECHA_LLEGADA, FECHA_SALIDA, PRECIO, LINEA_AEREA FROM VUELO WHERE NUMERO_VUELO = '" + numero + "'");
        while (rs.next()) {
            v[0] = rs.getString("NUMERO_VUELO");
            v[1] = rs.getString("AEROPUERTO_ORIGEN");
            v[2] = rs.getString("SALA");
            v[3] = rs.getString("HORA_SALIDA");
            v[4] = rs.getString("AEROPUERTO_DESTINO");
            v[5] = rs.getString("HORA_LLEGADA");
            v[6] = rs.getString("ESTADO");
            v[7] = rs.getString("TIPO");
            v[8] = rs.getString("AERONAVE");
            v[9] = rs.getString("TRIPULACION");
            v[10] = rs.getString("ASIENTOS_DISPONIBLES");
            v[11] = rs.getString("LINEA_AEREA");
            v[12] = rs.getString("FECHA_LLEGADA");
            v[13] = rs.getString("FECHA_SALIDA");
            v[14] = rs.getString("PRECIO");
        }
        return v;
    }

}
