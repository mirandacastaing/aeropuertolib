package castaing.miranda.bl.dao.vuelo;

import castaing.miranda.bl.dao.aeronave.Aeronave;
import castaing.miranda.bl.dao.aeropuerto.Aeropuerto;
import castaing.miranda.bl.dao.sala.Sala;
import castaing.miranda.bl.dao.tiquete.Tiquete;
import castaing.miranda.bl.dao.tripulacion.Tripulacion;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Vuelo {

    private Aeropuerto aeropuertoOrigen;
    private String horaLlegada;
    private String horaSalida;
    private char estado;
    private Aeropuerto aeropuertoDestino;
    private char tipo;
    private Aeronave tmpAeronave;
    private Sala tmpSala;
    private Tripulacion tmpTripulacion;
    private int numeroVuelo;
    private int asientosDisponibles;
    private ArrayList<Tiquete> tiquetes;
    private LocalDate fechaLlegada;
    private LocalDate fechaSalida;
    private double precio;

    public Vuelo() {
        horaLlegada = " ";
        horaSalida = " ";
        estado = ' ';
        tipo = ' ';
    }

    public Vuelo(Aeropuerto aeropuertoOrigen, String horaLlegada, String horaSalida, char estado,
                 Aeropuerto aeropuertoDestino, char tipo, Aeronave tmpAeronave, Sala tmpSala,
                 Tripulacion tmpTripulacion, int numeroVuelo, int asientosDisponibles, String fechaLlegada,
                 String fechaSalida, double precio) {
        this.aeropuertoOrigen = aeropuertoOrigen;
        this.horaLlegada = horaLlegada;
        this.horaSalida = horaSalida;
        this.estado = estado;
        this.aeropuertoDestino = aeropuertoDestino;
        this.tipo = tipo;
        this.tmpAeronave = tmpAeronave;
        this.tmpSala = tmpSala;
        this.tmpTripulacion = tmpTripulacion;
        this.numeroVuelo = numeroVuelo;
        this.asientosDisponibles = asientosDisponibles;
        this.tiquetes = new ArrayList<>();
        this.fechaLlegada = LocalDate.parse(fechaLlegada, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.fechaSalida = LocalDate.parse(fechaSalida, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.precio = precio;
    }

    public void agregarTiquete(Tiquete obj) {
        tiquetes.add(obj);
    }

    public Aeropuerto getAeropuertoOrigen() {
        return aeropuertoOrigen;
    }

    public void setAeropuertoOrigen(Aeropuerto aeropuertoOrigen) {
        this.aeropuertoOrigen = aeropuertoOrigen;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    public Aeropuerto getAeropuertoDestino() {
        return aeropuertoDestino;
    }

    public void setAeropuertoDestino(Aeropuerto aeropuertoDestino) {
        this.aeropuertoDestino = aeropuertoDestino;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    public Aeronave getTmpAeronave() {
        return tmpAeronave;
    }

    public void setTmpAeronave(Aeronave tmpAeronave) {
        this.tmpAeronave = tmpAeronave;
    }

    public Sala getTmpSala() {
        return tmpSala;
    }

    public void setTmpSala(Sala tmpSala) {
        this.tmpSala = tmpSala;
    }

    public Tripulacion getTmpTripulacion() {
        return tmpTripulacion;
    }

    public void setTmpTripulacion(Tripulacion tmpTripulacion) {
        this.tmpTripulacion = tmpTripulacion;
    }

    public int getNumeroVuelo() {
        return numeroVuelo;
    }

    public void setNumeroVuelo(int numeroVuelo) {
        this.numeroVuelo = numeroVuelo;
    }

    public int getAsientosDisponibles() {
        return asientosDisponibles;
    }

    public void setAsientosDisponibles(int asientosDisponibles) {
        this.asientosDisponibles = asientosDisponibles;
    }

    public ArrayList<Tiquete> getTiquetes() {
        return tiquetes;
    }

    public void setTiquetes(ArrayList<Tiquete> tiquetes) {
        this.tiquetes = tiquetes;
    }

    public LocalDate getFechaLlegada() {
        return fechaLlegada;
    }

    public void setFechaLlegada(String fechaLlegada) {
        this.fechaLlegada = LocalDate.parse(fechaLlegada, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public LocalDate getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = LocalDate.parse(fechaSalida, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Vuelo{" +
                "aeropuertoOrigen=" + aeropuertoOrigen +
                ", horaLlegada='" + horaLlegada + '\'' +
                ", horaSalida='" + horaSalida + '\'' +
                ", estado=" + estado +
                ", aeropuertoDestino=" + aeropuertoDestino +
                ", tipo=" + tipo +
                ", tmpAeronave=" + tmpAeronave +
                ", tmpSala=" + tmpSala +
                ", tmpTripulacion=" + tmpTripulacion +
                ", numeroVuelo=" + numeroVuelo +
                ", asientosDisponibles=" + asientosDisponibles +
                ", tiquetes=" + tiquetes +
                ", fechaLlegada=" + fechaLlegada +
                ", fechaSalida=" + fechaSalida +
                ", precio=" + precio +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vuelo vuelo = (Vuelo) o;
        return numeroVuelo == vuelo.numeroVuelo;
    }

}
