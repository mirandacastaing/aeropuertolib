package castaing.miranda.dl;

import castaing.miranda.bl.*;
import castaing.miranda.bl.dao.administrador.Administrador;
import castaing.miranda.bl.dao.aerea.linea.ILineaAereaDao;
import castaing.miranda.bl.dao.aerea.linea.LineaAerea;
import castaing.miranda.bl.dao.aeronave.Aeronave;
import castaing.miranda.bl.dao.aeronave.IAeronaveDao;
import castaing.miranda.bl.dao.aeropuerto.Aeropuerto;
import castaing.miranda.bl.dao.aeropuerto.IAeropuertoDao;
import castaing.miranda.bl.dao.factory.DaoFactory;
import castaing.miranda.bl.dao.pais.IPaisDao;
import castaing.miranda.bl.dao.pais.Pais;
import castaing.miranda.bl.dao.sala.ISalaDao;
import castaing.miranda.bl.dao.sala.Sala;
import castaing.miranda.bl.dao.tiquete.Tiquete;
import castaing.miranda.bl.dao.tripulacion.ITripulacionDao;
import castaing.miranda.bl.dao.tripulacion.Tripulacion;
import castaing.miranda.bl.dao.tripulante.MiembroTripulacion;
import castaing.miranda.bl.dao.ubicacion.IUbicacionDao;
import castaing.miranda.bl.dao.ubicacion.Ubicacion;
import castaing.miranda.bl.dao.usuario.Usuario;
import castaing.miranda.bl.dao.vuelo.IVueloDao;
import castaing.miranda.bl.dao.vuelo.Vuelo;
import castaing.miranda.excepciones.AeropuertoException;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

public class CapaLogica {

    ArrayList<Persona> personas = new ArrayList<>();
    ArrayList<Aeropuerto> aeropuertos = new ArrayList<>();
    ArrayList<LineaAerea> lineasAereas = new ArrayList<>();
    ArrayList<Pais> paises = new ArrayList<>();
    Persona personaIngresada;

    public boolean validarAdministradorAeropuertoRegistrados() {
        boolean registrados = false;
        for (Aeropuerto dato : aeropuertos) {
            if (dato.getTmpAdministrador() != null) {
                registrados = true;
            }
        }
        return registrados;
    }

    public void iniciarSesion(Persona obj) {
        personaIngresada = obj;
    }

    public boolean validarAdministradorAsignadoAeropuerto(Administrador administrador) {
        boolean asignacion = false;
        for (Aeropuerto dato : aeropuertos) {
            if (dato.getTmpAdministrador().equals(administrador)) {
                asignacion = true;
                break;
            }
        }
        return asignacion;
    }

    public Persona getPersonaIngresada() {
        return personaIngresada;
    }

    public void registrarPersona(Persona obj) {
        personas.add(obj);
        enviarContrasenna(obj.getCorreo(), obj.getContrasenna());
    }

    public void registrarMiembroTripulacion(MiembroTripulacion obj, String nombreComercialLineaAerea,
                                            String nombreTripulacion) {
        int posicionLineaAerea = 0, posicionTripulacion = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.getNombreComercial().equals(nombreComercialLineaAerea)) {
                for (Tripulacion dato2 : dato.getTripulaciones()) {
                    if (dato2.getNombre().equals(nombreTripulacion)) {
                        lineasAereas.get(posicionLineaAerea).getTripulaciones().get(posicionTripulacion).agregarMiembroTripulacion(obj);
                        break;
                    }
                    posicionTripulacion++;
                }
            }
            posicionLineaAerea++;
        }
        personas.add(obj);
        enviarContrasenna(obj.getCorreo(), obj.getContrasenna());
    }

    public void modificarPersona(Persona obj) {
        int posicion = 0;
        for (Persona dato : personas) {
            if (dato.equals(personaIngresada)) {
                personas.set(posicion, obj);
                personaIngresada = obj;
                break;
            }
            posicion++;
        }
    }

    public void modificarPais(Pais obj) {
        int posicion = 0;
        for (Pais dato : paises) {
            if (dato.equals(obj)) {
                paises.set(posicion, obj);
                break;
            }
            posicion++;
        }
    }

    public void eliminarPais(Pais obj) {
        int posicion = 0;
        for (Pais dato : paises) {
            if (dato.equals(obj)) {
                paises.remove(posicion);
                break;
            }
            posicion++;
        }
    }

    public void definirImpuesto(double impuesto) {
        int posicion = 0;
        for (Aeropuerto dato : aeropuertos) {
            if (dato.getTmpAdministrador().equals(personaIngresada)) {
                aeropuertos.get(posicion).setImpuesto(impuesto);
            }
            posicion++;
        }
    }

    public void modificarAeropuerto(Aeropuerto obj) {
        int posicion = 0;
        for (Aeropuerto dato : aeropuertos) {
            if (dato.equals(obj)) {
                aeropuertos.set(posicion, obj);
                break;
            }
            posicion++;
        }
    }

    public void eliminarAeropuerto(Aeropuerto obj) {
        int posicion = 0;
        for (Aeropuerto dato : aeropuertos) {
            if (dato.equals(obj)) {
                aeropuertos.remove(posicion);
                break;
            }
            posicion++;
        }
    }

    public void registrarUbicacion(Ubicacion obj) {
        buscarAeropuertoPorAdministrador().agregarUbicacion(obj);
    }

    public void modificarUbicacion(Ubicacion obj) {
        int posicion = 0;
        for (Ubicacion dato : buscarAeropuertoPorAdministrador().getUbicaciones()) {
            if (dato.equals(obj)) {
                buscarAeropuertoPorAdministrador().getUbicaciones().set(posicion, obj);
                break;
            }
            posicion++;
        }
    }

    public void eliminarUbicacion(Ubicacion obj) {
        int posicion = 0;
        for (Ubicacion dato : buscarAeropuertoPorAdministrador().getUbicaciones()) {
            if (dato.equals(obj)) {
                buscarAeropuertoPorAdministrador().getUbicaciones().remove(posicion);
                break;
            }
            posicion++;
        }
    }

    public void registrarSala(Sala obj, Ubicacion obj2) {
        ArrayList<Ubicacion> ubicaciones = buscarAeropuertoPorAdministrador().getUbicaciones();
        int posicion = 0;
        for (Ubicacion dato : ubicaciones) {
            if (dato.equals(obj2)) {
                buscarAeropuertoPorAdministrador().getUbicaciones().get(posicion).agregarSala(obj);
                break;
            }
            posicion++;
        }
    }

    public void modificarSala(Sala obj, Ubicacion obj2) {
        ArrayList<Ubicacion> ubicaciones = buscarAeropuertoPorAdministrador().getUbicaciones();
        int posicionUbicacion = 0, posicionSala = 0, posicionUbicacion2 = 0;
        for (Ubicacion dato : ubicaciones) {
            if (dato.equals(obj2)) {
                if (dato.getSalas().contains(obj)) {
                    for (Sala dato2 : dato.getSalas()) {
                        if (dato2.equals(obj)) {
                            buscarAeropuertoPorAdministrador().getUbicaciones().get(posicionUbicacion).getSalas().set(posicionSala, obj);
                            break;
                        }
                        posicionSala++;
                    }
                } else {
                    for (Ubicacion dato2 : ubicaciones) {
                        if (dato2.getSalas().contains(obj)) {
                            buscarAeropuertoPorAdministrador().getUbicaciones().get(posicionUbicacion2).getSalas().remove(obj);
                            buscarAeropuertoPorAdministrador().getUbicaciones().get(posicionUbicacion).agregarSala(obj);
                            break;
                        }
                        posicionUbicacion2++;
                    }
                }
            }
            posicionUbicacion++;
        }
    }

    public void eliminarSala(Sala obj) {
        ArrayList<Ubicacion> ubicaciones = buscarAeropuertoPorAdministrador().getUbicaciones();
        int posicionUbicacion = 0, posicionSala = 0;
        for (Ubicacion dato : ubicaciones) {
            for (Sala dato2 : dato.getSalas()) {
                if (dato2.equals(obj)) {
                    buscarAeropuertoPorAdministrador().getUbicaciones().get(posicionUbicacion).getSalas().remove(posicionSala);
                    break;
                }
                posicionSala++;
            }
            posicionUbicacion++;
        }
    }

    public void registrarLineaAerea(LineaAerea obj) {
        lineasAereas.add(obj);
    }

    public void modificarLineaAerea(LineaAerea obj) {
        int posicion = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.equals(obj)) {
                lineasAereas.set(posicion, obj);
                break;
            }
            posicion++;
        }
    }

    public void eliminarLineaAerea(LineaAerea obj) {
        int posicion = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.equals(obj)) {
                lineasAereas.remove(posicion);
                break;
            }
            posicion++;
        }
    }

    public void registrarVuelo(String nombreComercialLineaAerea, Vuelo obj) {
        if (obj.getTipo() == 'S') {
            obj.setEstado(asignarEstadoCorrecto(obj.getFechaSalida() + " " + obj.getHoraSalida()));
        } else {
            obj.setEstado(asignarEstadoCorrecto(obj.getFechaLlegada() + " " + obj.getHoraLlegada()));
        }
        int posicion = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.getNombreComercial().equals(nombreComercialLineaAerea)) {
                lineasAereas.get(posicion).agregarVuelo(obj);
                break;
            }
            posicion++;
        }
        if (obj.getTmpTripulacion().getTmpMiembrosTripulacion().size() != 0) {
            for (MiembroTripulacion dato : obj.getTmpTripulacion().getTmpMiembrosTripulacion()) {
                int posicionPersona = 0;
                for (Persona dato2 : personas) {
                    if (dato.getIdentificacion().equals(dato2.getIdentificacion())) {
                        ((MiembroTripulacion) personas.get(posicionPersona)).agregarVuelo(obj);
                    }
                    posicionPersona++;
                }
            }
        }
    }

    public void modificarVuelo(String nombreComercialLineaAerea, Vuelo obj) {
        int posicionLineaAerea = 0, posicionVuelo = 0, posicionLineaAerea2 = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.getNombreComercial().equals(nombreComercialLineaAerea)) {
                if (dato.getVuelos().contains(obj)) {
                    for (Vuelo dato2 : dato.getVuelos()) {
                        if (dato2.equals(obj)) {
                            obj.setAsientosDisponibles(dato2.getAsientosDisponibles());
                            lineasAereas.get(posicionLineaAerea).getVuelos().set(posicionVuelo, obj);
                            break;
                        }
                        posicionVuelo++;
                    }
                } else {
                    for (LineaAerea dato2 : lineasAereas) {
                        posicionVuelo = 0;
                        for (Vuelo dato3 : dato2.getVuelos()) {
                            if (dato3.equals(obj)) {
                                obj.setAsientosDisponibles(dato3.getAsientosDisponibles());
                                lineasAereas.get(posicionLineaAerea2).getVuelos().remove(obj);
                                lineasAereas.get(posicionLineaAerea).agregarVuelo(obj);
                                break;
                            }
                            posicionVuelo++;
                        }
                        posicionLineaAerea2++;
                    }
                }
            }
            posicionLineaAerea++;
        }
        if (obj.getTmpTripulacion().getTmpMiembrosTripulacion().size() != 0) {
            for (MiembroTripulacion dato : obj.getTmpTripulacion().getTmpMiembrosTripulacion()) {
                int posicionPersona = 0;
                for (Persona dato2 : personas) {
                    if (dato.getIdentificacion().equals(dato2.getIdentificacion())) {
                        int posicionVuelo2 = 0;
                        for (Vuelo dato3 : ((MiembroTripulacion) dato2).getVuelos()) {
                            if (dato3.equals(obj)) {
                                ((MiembroTripulacion) personas.get(posicionPersona)).getVuelos().set(posicionVuelo2, obj);
                            }
                            posicionVuelo2++;
                        }
                    }
                    posicionPersona++;
                }
            }
        }
    }

    public void eliminarVuelo(Vuelo obj) throws AeropuertoException {
        int posicionMiembroTripulacion = 0;
        for (LineaAerea dato : lineasAereas) {
            int posicion = 0;
            for (Vuelo dato2 : dato.getVuelos()) {
                if (dato2.equals(obj)) {
                    //buscarLineaAereaPorCedula(dato.getCedula()).getVuelos().remove(posicion);
                    break;
                }
                posicion++;
            }
        }
        if (obj.getTmpTripulacion().getTmpMiembrosTripulacion().size() != 0) {
            for (MiembroTripulacion dato : obj.getTmpTripulacion().getTmpMiembrosTripulacion()) {
                int posicionPersona = 0;
                for (Persona dato2 : personas) {
                    if (dato.getIdentificacion().equals(dato2.getIdentificacion())) {
                        int posicionVuelo2 = 0;
                        for (Vuelo dato3 : ((MiembroTripulacion) dato2).getVuelos()) {
                            if (dato3.equals(obj)) {
                                ((MiembroTripulacion) personas.get(posicionPersona)).getVuelos().remove(posicionVuelo2);
                            }
                            posicionVuelo2++;
                        }
                    }
                    posicionPersona++;
                }
            }
        }
    }

    public void registrarTripulacion(String nombreComercialLineaAerea, Tripulacion obj) {
        int posicion = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.getNombreComercial().equals(nombreComercialLineaAerea)) {
                lineasAereas.get(posicion).agregarTripulacion(obj);
                break;
            }
            posicion++;
        }
    }

    public void modificarTripulacion(String nombreComercialLineaAerea, Tripulacion obj) {
        int posicionLineaAerea = 0, posicionTripulacion = 0, posicionLineaAerea2 = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.getNombreComercial().equals(nombreComercialLineaAerea)) {
                if (dato.getTripulaciones().contains(obj)) {
                    for (Tripulacion dato2 : dato.getTripulaciones()) {
                        if (dato2.equals(obj)) {
                            lineasAereas.get(posicionLineaAerea).getTripulaciones().set(posicionTripulacion, obj);
                            break;
                        }
                        posicionTripulacion++;
                    }
                } else {
                    for (LineaAerea dato2 : lineasAereas) {
                        if (dato2.getTripulaciones().contains(obj)) {
                            lineasAereas.get(posicionLineaAerea2).getTripulaciones().remove(obj);
                            lineasAereas.get(posicionLineaAerea).agregarTripulacion(obj);
                            break;
                        }
                        posicionLineaAerea2++;
                    }
                }
            }
            posicionLineaAerea++;
        }
    }

    public void eliminarTripulacion(Tripulacion obj) throws AeropuertoException {
        for (LineaAerea dato : lineasAereas) {
            int posicion = 0;
            for (Tripulacion dato2 : dato.getTripulaciones()) {
                if (dato2.equals(obj)) {
                    //buscarLineaAereaPorCedula(dato.getCedula()).getTripulaciones().remove(posicion);
                    break;
                }
                posicion++;
            }
        }
    }

    public void registrarAeronave(String nombreComercialLineaAerea, Aeronave obj) {
        int posicion = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.getNombreComercial().equals(nombreComercialLineaAerea)) {
                lineasAereas.get(posicion).agregarAeronave(obj);
                break;
            }
            posicion++;
        }
    }

    public void modificarAeronave(String nombreComercialLineaAerea, Aeronave obj) {
        int posicionLineaAerea = 0, posicionAeronave = 0, posicionLineaAerea2 = 0;
        for (LineaAerea dato : lineasAereas) {
            if (dato.getNombreComercial().equals(nombreComercialLineaAerea)) {
                if (dato.getAeronaves().contains(obj)) {
                    for (Aeronave dato2 : dato.getAeronaves()) {
                        if (dato2.equals(obj)) {
                            lineasAereas.get(posicionLineaAerea).getAeronaves().set(posicionAeronave, obj);
                            break;
                        }
                        posicionAeronave++;
                    }
                } else {
                    for (LineaAerea dato2 : lineasAereas) {
                        if (dato2.getAeronaves().contains(obj)) {
                            lineasAereas.get(posicionLineaAerea2).getAeronaves().remove(obj);
                            lineasAereas.get(posicionLineaAerea).agregarAeronave(obj);
                            break;
                        }
                        posicionLineaAerea2++;
                    }
                }
            }
            posicionLineaAerea++;
        }
    }

    public void eliminarAeronave(Aeronave obj) throws AeropuertoException {
        for (LineaAerea dato : lineasAereas) {
            int posicion = 0;
            for (Aeronave dato2 : dato.getAeronaves()) {
                if (dato2.equals(obj)) {
                    //buscarLineaAereaPorCedula(dato.getCedula()).getAeronaves().remove(posicion);
                    break;
                }
                posicion++;
            }
        }
    }

    public void registrarTiquete(String numeroAsiento, double precio, String numeroVuelo, char tipoAsiento)
            throws AeropuertoException {
        try {
            enviarFactura(numeroVuelo, new Tiquete(LocalDate.now(), numeroAsiento, precio, tipoAsiento));
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] getUbicaciones() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUbicacionDao daoUbicacion = factory.getUbicacionDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuerto = factory2.getAeropuertoDao();
        String[][] info = null;
        try {
            ArrayList<Ubicacion> ubicaciones = daoUbicacion.listar(daoAeropuerto.getAeropuertoDeAdministrador(personaIngresada.getCorreo())[1]);
            info = new String[ubicaciones.size()][2];
            int posicion = 0;
            for (Ubicacion dato : ubicaciones) {
                info[posicion][0] = Character.toString(dato.getPlanta());
                info[posicion][1] = dato.getDescripcion();
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getSalas() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory.getSalaDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuerto = factory2.getAeropuertoDao();
        String[][] info = null;
        try {
            ArrayList<String[]> salas = daoSala.listar(daoAeropuerto.getAeropuertoDeAdministrador(personaIngresada.getCorreo())[1]);
            info = new String[salas.size()][3];
            int posicion = 0;
            for (String[] dato : salas) {
                info[posicion][0] = dato[1];
                info[posicion][1] = dato[0];
                info[posicion][2] = dato[2];
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getPaises() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        String[][] info = null;
        try {
            ArrayList<Pais> lista = daoPais.listar();
            info = new String[lista.size()][3];
            int posicion = 0;
            for (Pais dato : lista) {
                info[posicion][0] = dato.getCodigo();
                info[posicion][1] = dato.getNombre();
                info[posicion][2] = dato.getAbreviatura();
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getLineasAereas() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory.getLineaAereaDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory2.getPaisDao();
        String[][] info;
        int posicion = 0;
        try {
            ArrayList<String[]> lista = daoLineaAerea.listar();
            info = new String[lista.size()][5];
            for (String[] dato : lista) {
                info[posicion][0] = dato[4];
                info[posicion][1] = dato[1];
                info[posicion][2] = dato[0];
                info[posicion][3] = daoPais.buscarPaisPorCodigo(dato[3]).getNombre();
                info[posicion][4] = dato[2];
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getVuelos() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO = factory2.getAeropuertoDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory3.getSalaDao();
        DaoFactory factory4 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD = factory4.getAeropuertoDao();
        DaoFactory factory5 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory5.getTripulacionDao();
        DaoFactory factory6 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory6.getLineaAereaDao();
        DaoFactory factory7 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisO = factory7.getPaisDao();
        DaoFactory factory8 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO2 = factory8.getAeropuertoDao();
        DaoFactory factory9 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisD = factory9.getPaisDao();
        DaoFactory factory10 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD2 = factory10.getAeropuertoDao();
        String[][] info;
        int posicion = 0;
        try {
            ArrayList<String[]> lista = daoVuelo.listar();
            info = new String[lista.size()][17];
            for (String[] dato : lista) {
                info[posicion][0] = dato[0];
                info[posicion][1] = daoAeropuertoO.buscarAeropuertoPorCodigo(dato[1])[1];
                info[posicion][2] = daoSala.buscarSalaPorCodigo(dato[2])[1];
                info[posicion][3] = dato[3];
                info[posicion][4] = daoAeropuertoD.buscarAeropuertoPorCodigo(dato[4])[1];
                info[posicion][5] = dato[5];
                info[posicion][6] = dato[6];
                info[posicion][7] = dato[7];
                info[posicion][8] = dato[8];
                info[posicion][9] = daoTripulacion.buscarTripulacionPorCodigo(dato[9])[1];
                info[posicion][10] = dato[10];
                info[posicion][11] = daoLineaAerea.buscarLineaAereaPorCedula(dato[11])[1];
                info[posicion][12] = daoPaisO.buscarPaisPorCodigo(daoAeropuertoO2.buscarAeropuertoPorCodigo(dato[1])[2]).getNombre();
                info[posicion][13] = dato[12];
                info[posicion][14] = dato[13];
                info[posicion][15] = daoPaisD.buscarPaisPorCodigo(daoAeropuertoD2.buscarAeropuertoPorCodigo(dato[4])[2]).getNombre();
                info[posicion][16] = dato[14];
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getVuelosLlegada() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO = factory2.getAeropuertoDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory3.getSalaDao();
        DaoFactory factory4 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD = factory4.getAeropuertoDao();
        DaoFactory factory5 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory5.getTripulacionDao();
        DaoFactory factory6 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory6.getLineaAereaDao();
        DaoFactory factory7 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisO = factory7.getPaisDao();
        DaoFactory factory8 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO2 = factory8.getAeropuertoDao();
        DaoFactory factory9 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisD = factory9.getPaisDao();
        DaoFactory factory10 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD2 = factory10.getAeropuertoDao();
        String[][] info;
        int posicion = 0;
        try {
            ArrayList<String[]> lista = daoVuelo.listarDeLlegada();
            info = new String[lista.size()][16];
            for (String[] dato : lista) {
                info[posicion][0] = dato[0];
                info[posicion][1] = daoAeropuertoO.buscarAeropuertoPorCodigo(dato[1])[1];
                info[posicion][2] = daoSala.buscarSalaPorCodigo(dato[2])[1];
                info[posicion][3] = dato[3];
                info[posicion][4] = daoAeropuertoD.buscarAeropuertoPorCodigo(dato[4])[1];
                info[posicion][5] = dato[5];
                info[posicion][6] = dato[6];
                info[posicion][7] = dato[7];
                info[posicion][8] = dato[8];
                info[posicion][9] = daoTripulacion.buscarTripulacionPorCodigo(dato[9])[1];
                info[posicion][10] = dato[10];
                info[posicion][11] = daoLineaAerea.buscarLineaAereaPorCedula(dato[11])[1];
                info[posicion][12] = daoPaisO.buscarPaisPorCodigo(daoAeropuertoO2.buscarAeropuertoPorCodigo(dato[1])[2]).getNombre();
                info[posicion][13] = dato[12];
                info[posicion][14] = dato[13];
                info[posicion][15] = daoPaisD.buscarPaisPorCodigo(daoAeropuertoD2.buscarAeropuertoPorCodigo(dato[4])[2]).getNombre();
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getVuelosSalida() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO = factory2.getAeropuertoDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory3.getSalaDao();
        DaoFactory factory4 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD = factory4.getAeropuertoDao();
        DaoFactory factory5 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory5.getTripulacionDao();
        DaoFactory factory6 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory6.getLineaAereaDao();
        DaoFactory factory7 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisO = factory7.getPaisDao();
        DaoFactory factory8 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO2 = factory8.getAeropuertoDao();
        DaoFactory factory9 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisD = factory9.getPaisDao();
        DaoFactory factory10 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD2 = factory10.getAeropuertoDao();
        String[][] info;
        int posicion = 0;
        try {
            ArrayList<String[]> lista = daoVuelo.listarDeSalida();
            info = new String[lista.size()][16];
            for (String[] dato : lista) {
                info[posicion][0] = dato[0];
                info[posicion][1] = daoAeropuertoO.buscarAeropuertoPorCodigo(dato[1])[1];
                info[posicion][2] = daoSala.buscarSalaPorCodigo(dato[2])[1];
                info[posicion][3] = dato[3];
                info[posicion][4] = daoAeropuertoD.buscarAeropuertoPorCodigo(dato[4])[1];
                info[posicion][5] = dato[5];
                info[posicion][6] = dato[6];
                info[posicion][7] = dato[7];
                info[posicion][8] = dato[8];
                info[posicion][9] = daoTripulacion.buscarTripulacionPorCodigo(dato[9])[1];
                info[posicion][10] = dato[10];
                info[posicion][11] = daoLineaAerea.buscarLineaAereaPorCedula(dato[11])[1];
                info[posicion][12] = daoPaisO.buscarPaisPorCodigo(daoAeropuertoO2.buscarAeropuertoPorCodigo(dato[1])[2]).getNombre();
                info[posicion][13] = dato[12];
                info[posicion][14] = dato[13];
                info[posicion][15] = daoPaisD.buscarPaisPorCodigo(daoAeropuertoD2.buscarAeropuertoPorCodigo(dato[4])[2]).getNombre();
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getVuelosMiembroTripulacion() {
        String[][] info;
        int posicion = 0;
        info = new String[((MiembroTripulacion) personaIngresada).getVuelos().size()][16];
        for (LineaAerea dato : lineasAereas) {
            for (Vuelo dato2 : dato.getVuelos()) {
                if (((MiembroTripulacion) personaIngresada).getVuelos().contains(dato2)) {
                    info[posicion][0] = Integer.toString(dato2.getNumeroVuelo());
                    info[posicion][1] = dato2.getAeropuertoOrigen().getNombre();
                    info[posicion][2] = dato2.getTmpSala().getNombre();
                    info[posicion][3] = dato2.getHoraSalida();
                    info[posicion][4] = dato2.getAeropuertoDestino().getNombre();
                    info[posicion][5] = dato2.getHoraLlegada();
                    info[posicion][6] = Character.toString(dato2.getEstado());
                    info[posicion][7] = Character.toString(dato2.getTipo());
                    info[posicion][8] = dato2.getTmpAeronave().getPlaca();
                    info[posicion][9] = dato2.getTmpTripulacion().getNombre();
                    info[posicion][10] = Integer.toString(dato2.getAsientosDisponibles());
                    info[posicion][11] = dato.getNombreComercial();
                    info[posicion][12] = dato2.getAeropuertoOrigen().getTmpPais().getNombre();
                    info[posicion][13] = dato2.getFechaLlegada().toString();
                    info[posicion][14] = dato2.getFechaSalida().toString();
                    info[posicion][15] = dato2.getAeropuertoDestino().getTmpPais().getNombre();
                    posicion++;
                }
            }
        }
        return info;
    }

    public String[][] getTripulaciones() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory.getTripulacionDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        String[][] info;
        int posicion = 0;
        try {
            ArrayList<String[]> lista = daoTripulacion.listar();
            info = new String[lista.size()][3];
            for (String[] dato : lista) {
                info[posicion][0] = dato[0];
                info[posicion][1] = dato[1];
                info[posicion][2] = daoLineaAerea.buscarLineaAereaPorCedula(dato[2])[1];
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getAeronaves() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronaveDao daoAeronave = factory.getAeronaveDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        String[][] info;
        int posicion = 0;
        try {
            ArrayList<String[]> lista = daoAeronave.listar();
            info = new String[lista.size()][5];
            for (String[] dato : lista) {
                info[posicion][0] = daoLineaAerea.buscarLineaAereaPorCedula(dato[4])[1];
                info[posicion][1] = dato[0];
                info[posicion][2] = dato[1];
                info[posicion][3] = dato[2];
                info[posicion][4] = dato[3];
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public String[][] getUsuarios() {
        String[][] info;
        int tamanno = 0, posicion = 0;
        for (Persona dato : personas) {
            if (dato instanceof Usuario) {
                tamanno++;
            }
        }
        info = new String[tamanno][11];
        for (Persona dato : personas) {
            if (dato instanceof Usuario) {
                info[posicion][0] = dato.getIdentificacion();
                info[posicion][1] = dato.getNombre();
                info[posicion][2] = dato.getApellidos();
                info[posicion][3] = ((Usuario) dato).getFechaNacimiento().toString();
                info[posicion][4] = dato.getNacionalidad();
                info[posicion][5] = dato.getCorreo();
                info[posicion][6] = dato.getContrasenna();
                info[posicion][7] = ((Usuario) dato).getDistrito();
                info[posicion][8] = ((Usuario) dato).getCanton();
                info[posicion][9] = ((Usuario) dato).getProvincia();
                info[posicion][10] = dato.getDireccion();
                posicion++;
            }
        }
        return info;
    }

    public String[][] getAeropuertos() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuerto = factory.getAeropuertoDao();
        String[][] info;
        int posicion = 0;
        try {
            ArrayList<String[]> lista = daoAeropuerto.listar();
            info = new String[lista.size()][4];
            for (String[] dato : lista) {
                info[posicion][0] = dato[1];
                info[posicion][1] = dato[0];
                info[posicion][2] = dato[2];
                info[posicion][3] = dato[3];
                posicion++;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return info;
    }

    public Aeropuerto obtenerAeropuerto() {
        Aeropuerto tmpAeropuerto = new Aeropuerto();
        for (Aeropuerto dato : aeropuertos) {
            if (dato.getTmpAdministrador().equals(personaIngresada)) {
                tmpAeropuerto = dato;
            }
        }
        return tmpAeropuerto;
    }

    public Persona buscarPersonaPorCorreo(String correo) {
        Persona tmpPersona = new Persona();
        for (Persona dato : personas) {
            if (dato.getCorreo().equals(correo)) {
                tmpPersona = dato;
                break;
            }
        }
        return tmpPersona;
    }

    public Aeropuerto buscarAeropuertoPorAdministrador() {
        Aeropuerto tmpAeropuerto = new Aeropuerto();
        for (Aeropuerto dato : aeropuertos) {
            if (dato.getTmpAdministrador().equals(personaIngresada)) {
                tmpAeropuerto = dato;
                break;
            }
        }
        return tmpAeropuerto;
    }

    public Aeropuerto buscarAeropuertoPorCodigo(String codigo) {
        Aeropuerto tmpAeropuerto = new Aeropuerto();
        for (Aeropuerto dato : aeropuertos) {
            if (dato.getCodigo().equals(codigo)) {
                tmpAeropuerto = dato;
                break;
            }
        }
        return tmpAeropuerto;
    }

    public Aeropuerto buscarAeropuertoPorNombre(String nombre) {
        Aeropuerto tmpAeropuerto = new Aeropuerto();
        for (Aeropuerto dato : aeropuertos) {
            if (dato.getNombre().equals(nombre)) {
                tmpAeropuerto = dato;
                break;
            }
        }
        return tmpAeropuerto;
    }

    public Ubicacion buscarUbicacionPorPlanta(char planta) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUbicacionDao daoUbicacion = factory.getUbicacionDao();
        try {
            return daoUbicacion.buscarUbicacionPorPlanta(planta);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public Pais buscarPaisPorCodigo(String codigo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        try {
            return daoPais.buscarPaisPorCodigo(codigo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public Pais buscarPaisPorNombre(String nombre) {
        Pais tmpPais = new Pais();
        for (Pais dato : paises) {
            if (dato.getNombre().equals(nombre)) {
                tmpPais = dato;
                break;
            }
        }
        return tmpPais;
    }

    public String[] buscarLineaAereaPorCedula(String cedula) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory.getLineaAereaDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory2.getPaisDao();
        try {
            String[] l = daoLineaAerea.buscarLineaAereaPorCedula(cedula);
            l[3] = daoPais.buscarPaisPorCodigo(l[3]).getNombre();
            return l;
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public Object[] buscarAeronavePorPlaca(String placa) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronaveDao daoAeronave = factory.getAeronaveDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        Object[] objs = new Object[2];
        Aeronave tmpAeronave;
        try {
            String[] a = daoAeronave.buscarAeronavePorPlaca(placa);
            a[4] = daoLineaAerea.buscarLineaAereaPorCedula(a[4])[1];
            tmpAeronave = new Aeronave(a[0], a[1], a[2], Integer.parseInt(a[3]));
            objs[0] = tmpAeronave;
            objs[1] = a;
            return objs;
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[] buscarSalaPorCodigo(String codigo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory.getSalaDao();
        try {
            return daoSala.buscarSalaPorCodigo(codigo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public Sala buscarSalaPorNombre(String nombre) {
        Sala tmpSala = new Sala();
        ArrayList<Ubicacion> ubicaciones = buscarAeropuertoPorAdministrador().getUbicaciones();
        for (Ubicacion dato : ubicaciones) {
            for (Sala dato2 : dato.getSalas()) {
                if (dato2.getNombre().equals(nombre)) {
                    tmpSala = dato2;
                    break;
                }
            }
        }
        return tmpSala;
    }

    public Object[] buscarTripulacionPorCodigo(String codigo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory.getTripulacionDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        Object[] objs = new Object[2];
        Tripulacion tmpTripulacion;
        try {
            String[] t = daoTripulacion.buscarTripulacionPorCodigo(codigo);
            t[2] = daoLineaAerea.buscarLineaAereaPorCedula(t[2])[1];
            tmpTripulacion = new Tripulacion(t[0], t[1]);
            objs[0] = tmpTripulacion;
            objs[1] = t;
            return objs;
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public Tripulacion buscarTripulacionPorNombre(String nombre) {
        Tripulacion tmpTripulacion = new Tripulacion();
        for (LineaAerea dato : lineasAereas) {
            for (Tripulacion dato2 : dato.getTripulaciones()) {
                if (dato2.getNombre().equals(nombre)) {
                    tmpTripulacion = dato2;
                    break;
                }
            }
        }
        return tmpTripulacion;
    }

    public Object[] buscarVueloPorNumero(String numero) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO = factory2.getAeropuertoDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory3.getSalaDao();
        DaoFactory factory4 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD = factory4.getAeropuertoDao();
        DaoFactory factory5 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory5.getTripulacionDao();
        DaoFactory factory6 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory6.getLineaAereaDao();
        DaoFactory factory7 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisO = factory7.getPaisDao();
        DaoFactory factory8 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO2 = factory8.getAeropuertoDao();
        DaoFactory factory9 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisD = factory9.getPaisDao();
        DaoFactory factory10 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD2 = factory10.getAeropuertoDao();
        Object[] objs = new Object[2];
        Vuelo tmpVuelo;
        try {
            String[] v = daoVuelo.buscarVueloPorNumero(numero);
            String[] infoVuelo = new String[14];
            infoVuelo[0] = v[0];
            infoVuelo[1] = daoAeropuertoO.buscarAeropuertoPorCodigo(v[1])[1];
            infoVuelo[2] = v[3];
            infoVuelo[3] = daoAeropuertoD.buscarAeropuertoPorCodigo(v[4])[1];
            infoVuelo[4] = v[5];
            infoVuelo[5] = v[7];
            infoVuelo[6] = daoSala.buscarSalaPorCodigo(v[2])[1];
            infoVuelo[7] = v[8];
            infoVuelo[8] = daoTripulacion.buscarTripulacionPorCodigo(v[9])[1];
            infoVuelo[9] = daoLineaAerea.buscarLineaAereaPorCedula(v[11])[1];
            infoVuelo[10] = v[6];
            infoVuelo[11] = v[13];
            infoVuelo[12] = v[12];
            infoVuelo[13] = v[14];
            objs[1] = infoVuelo;
            return objs;
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String generarContrasenna() {
        String contrasenna = "";
        char[] contrasennaNueva = new char[8];
        String contrasennaFinal = "";
        String caracteres = "BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?0123456789";
        String mayusculas = "BCDFGHJKLMNPQRSTVWXYZ";
        String minusculas = "bcdfghjklmnpqrstvwxyz";
        String caracteresEspeciales = "~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
        String numeros = "0123456789";
        Random random = new Random();

        contrasenna += mayusculas.charAt(random.nextInt(mayusculas.length()));
        contrasenna += minusculas.charAt(random.nextInt(minusculas.length()));
        contrasenna += caracteresEspeciales.charAt(random.nextInt(caracteresEspeciales.length()));
        contrasenna += numeros.charAt(random.nextInt(numeros.length()));

        for (int i = 0; i < 4; i++) {
            contrasenna += caracteres.charAt(random.nextInt(caracteres.length()));
        }

        for (char dato : contrasenna.toCharArray()) {
            int posicion;
            do {
                posicion = random.nextInt(8);
            } while (contrasennaNueva[posicion] != '\0');
            contrasennaNueva[posicion] = dato;
        }

        for (char dato : contrasennaNueva) {
            contrasennaFinal += Character.toString(dato);
        }

        return contrasennaFinal;
    }


    public void enviarContrasenna(String correo, String contrasenna) {
        String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

        Properties props = System.getProperties();

        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.store.protocol", "pop3");
        props.put("mail.transport.protocol", "smtp");

        final String username = "mcastaingk@ucenfotec.ac.cr";//
        final String password = "LunaStruck250200";
        final String EMAIL_TEXT = "<html>\n" +
                "        <head>\n" +
                "            <link href=\"https://fonts.googleapis.com/css?family=Montserrat\" rel=\"stylesheet\">\n" +
                "            <style>\n" +
                "                main{\n" +
                "                    background-size: cover;\n" +
                "                    height: max-content;\n" +
                "                    padding-top: 80px;\n" +
                "                }\n" +
                "                section{\n" +
                "                    float: center;\n" +
                "                    margin: 0 auto;\n" +
                "                    background: linear-gradient(to top, #1488cc, #2b32b2);\n" +
                "                    padding: 25px;\n" +
                "                    color: white;\n" +
                "                    margin-top: 25px;\n" +
                "                    margin-left: 40px;\n" +
                "                    margin-right: 40px;\n" +
                "                    margin-bottom: 35px;\n" +
                "                    font-family: 'Montserrat', sans-serif; text-decoration:none !important; " +
                "                    text-decoration:none !important; text-decoration:none; text-underline:none; " +
                "                    border-width:0px; border:0px;" +
                "                }\n" +
                "                #sct1 div{\n" +
                "                    position: absolute;\n" +
                "                    text-align: center;\n" +
                "                }\n" +
                "                #sct1 h1{\n" +
                "                    font-size: 36px;\n" +
                "                    font-weight: bold;\n" +
                "                }" +
                "                #sct1 img{\n" +
                "                    width: 200px;\n" +
                "                    height: 200px;\n" +
                "                }\n" +
                "                #sct1 p, #sct1 a{\n" +
                "                    font-size: 20px;\n" +
                "                    text-decoration:none !important; text-decoration:none; text-underline:none; " +
                "                    border-width:0px; border:0px;" +
                "                    color: white;" +
                "                }\n" +
                "                #sct1 b{\n" +
                "                    font-weight: bold;\n" +
                "                    font-size: 30px;\n" +
                "                }\n" +
                "           </style>\n" +
                "        </head>\n" +
                "        <body>\n" +
                "            <main>\n" +
                "               <section id=\"sct1\">\n" +
                "                    <div>\n" +
                "                        <img src=\"cid:image\"/>\n" +
                "                        <h1>\n" +
                "                            Aeropuerto Internacional de Orotina\n" +
                "                        </h1>\n" +
                "                        <p>\n" +
                "                            <b>¡Bienvenido(a) a la aplicación!</b>\n" +
                "                        </p>\n" +
                "                        <p>\n" +
                "                            Credencial: \n" + correo +
                "                        </p>\n" +
                "                        <p>\n" +
                "                            Contraseña: \n<b>" + contrasenna + "</b>" +
                "                        </p>\n" +
                "                        <p>\n" +
                "                            Ubicación: Orotina, Alajuela, Costa Rica\n" +
                "                        </p>\n" +
                "                        <p>\n" +
                "                            Tel.: +(506) 8939-7940\n" +
                "                        </p>\n" +
                "                        <p>\n" +
                "                            Correo: info@aeropuertoorotina.com\n" +
                "                        </p>\n" +
                "                    </div>\n" +
                "                </section>\n" +
                "            </main>\n" +
                "        </body>\n" +
                "    </html>";

        try {
            Session session = Session.getDefaultInstance(props, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress("info@aeropuertoorotina.com"));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(correo, false));
            msg.setSubject("Envío de contraseña");

            MimeBodyPart p1 = new MimeBodyPart();
            p1.setDataHandler(new DataHandler(new HTMLDataSource(EMAIL_TEXT)));

            MimeBodyPart p2 = new MimeBodyPart();

            FileDataSource fds = new FileDataSource("C:\\Users\\miran\\Documents\\IntellijIDEAProjects\\AeropuertoLib\\logo.png");
            p2.setDataHandler(new DataHandler(fds));
            p2.setFileName(fds.getName());
            p2.setHeader("Content-ID","<image>");

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(p1);
            mp.addBodyPart(p2);

            msg.setContent(mp);

            msg.setSentDate(new Date());

            Transport.send(msg);
        } catch (MessagingException e) {
            System.out.println("Error de envío, causa: " + e);
        }
    }

    public char asignarEstadoCorrecto(String fechaCompleta) {
        char estado;
        LocalDate fecha = LocalDate.parse(fechaCompleta, DateTimeFormatter.ofPattern("yyyy-MM-dd H:m"));
        if (fecha.isAfter(LocalDate.now())) {
            estado = 'R';
        } else {
            estado = 'T';
        }
        return estado;
    }

    public void enviarFactura(String numeroVuelo, Tiquete tmpTiquete) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO = factory2.getAeropuertoDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory3.getSalaDao();
        DaoFactory factory4 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD = factory4.getAeropuertoDao();
        DaoFactory factory7 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisO = factory7.getPaisDao();
        DaoFactory factory8 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO2 = factory8.getAeropuertoDao();
        DaoFactory factory9 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPaisD = factory9.getPaisDao();
        DaoFactory factory10 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD2 = factory10.getAeropuertoDao();
        DaoFactory factory11 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO3 = factory11.getAeropuertoDao();
        DaoFactory factory12 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD3 = factory12.getAeropuertoDao();
        DaoFactory factory13 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo2 = factory13.getVueloDao();

        try {
            String tipoAsiento, origen, destino;
            double impuesto;
            String[] v = daoVuelo.buscarVueloPorNumero(numeroVuelo);
            String[] aO = daoAeropuertoO.buscarAeropuertoPorCodigo(v[1]);
            String[] aD = daoAeropuertoO.buscarAeropuertoPorCodigo(v[4]);
            Pais pO = daoPaisO.buscarPaisPorCodigo(aO[2]);
            Pais pD = daoPaisD.buscarPaisPorCodigo(aD[2]);
            String[] s = daoSala.buscarSalaPorCodigo(v[2]);
            System.out.println("1");
            daoVuelo2.restarAsiento(numeroVuelo);
            System.out.println("2");
            System.out.println("3");

            switch (tmpTiquete.getTipoAsiento()) {
                case 'V':
                    tipoAsiento = "Ventana";
                    break;
                case 'M':
                    tipoAsiento = "Medio";
                    break;
                case 'P':
                    tipoAsiento = "Pasillo";
                    break;
                default:
                    tipoAsiento = "Indefinido";
                    break;
            }

            System.out.println(v[7]);
            if (v[7].equals("L")) {
                System.out.println("4");
                impuesto = Double.parseDouble(aO[4]);
                System.out.println("5");
                origen = aO[1] + " (" + pO.getAbreviatura() + ")";
                System.out.println("8");
                destino = "Aeropuerto Internacional de Orotina (CR)";
            } else {
                System.out.println("4");
                System.out.println(aD[4]);
                impuesto = Double.parseDouble(aD[4]);
                System.out.println("5");
                origen = "Aeropuerto Internacional de Orotina (CR)";
                System.out.println("8");
                destino = aD[1] + " (" + pD.getAbreviatura() + ")";
            }

            String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

            Properties props = System.getProperties();

            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            props.setProperty("mail.smtp.port", "465");
            props.setProperty("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.auth", "true");
            props.put("mail.debug", "true");
            props.put("mail.store.protocol", "pop3");
            props.put("mail.transport.protocol", "smtp");

            final String username = "mcastaingk@ucenfotec.ac.cr";//
            final String password = "LunaStruck250200";
            final String EMAIL_TEXT = "<html>\n" +
                    "        <head>\n" +
                    "            <link href=\"https://fonts.googleapis.com/css?family=Montserrat\" rel=\"stylesheet\">\n" +
                    "            <style>\n" +
                    "                main{\n" +
                    "                    background-size: cover;\n" +
                    "                    height: max-content;\n" +
                    "                    padding-top: 80px;\n" +
                    "                }\n" +
                    "                section{\n" +
                    "                    float: center;\n" +
                    "                    margin: 0 auto;\n" +
                    "                    padding: 25px;\n" +
                    "                    color: white;\n" +
                    "                    margin-top: 25px;\n" +
                    "                    margin-left: 40px;\n" +
                    "                    margin-right: 40px;\n" +
                    "                    margin-bottom: 35px;\n" +
                    "                    font-family: 'Montserrat', sans-serif; text-decoration:none !important;\n" +
                    "                    text-decoration:none !important; text-decoration:none; text-underline:none;\n" +
                    "                    border-width:0px; border:0px;\n" +
                    "                }\n" +
                    "#sct1 {background: linear-gradient(to top, #1488cc, #2b32b2);}" +
                    "#sct2 {background: linear-gradient(to bottom, #e52d27, #b31217);}" +
                    "                #sct1 div{\n" +
                    "                    position: absolute;\n" +
                    "                    text-align: center;\n" +
                    "                }\n" +
                    "                #sct1 h1{\n" +
                    "                    font-size: 36px;\n" +
                    "                    font-weight: bold;\n" +
                    "                }\n" +
                    "                #sct2 h2{\n" +
                    "                    font-size: 30px;\n" +
                    "                    font-weight: bold;\n" +
                    "                    text-align: center;\n" +
                    "                }\n" +
                    "                #sct1 img{\n" +
                    "                    width: 200px;\n" +
                    "                    height: 200px;\n" +
                    "                }\n" +
                    "                #sct1 p, #sct1 a{\n" +
                    "                    font-size: 20px;\n" +
                    "                    text-decoration:none !important; text-decoration:none; text-underline:none;\n" +
                    "                    border-width:0px; border:0px;\n" +
                    "                    color: white;" +
                    "                }\n" +
                    "                #sct1 b{\n" +
                    "                    font-weight: bold;\n" +
                    "                    font-size: 30px;\n" +
                    "                }\n" +
                    "                #sct2 table{\n" +
                    "                    border-collapse: collapse;\n" +
                    "                    width: 100%;\n" +
                    "                    background-color: #dddddd83;\n" +
                    "                    text-align: center;\n" +
                    "                    font-family: 'Montserrat', sans-serif;\n" +
                    "                    text-decoration: none;\n" +
                    "                }\n" +
                    "                #sct2 table tr td, #sct2 table tr th{\n" +
                    "                    border: 1px solid #cccccc;\n" +
                    "                    padding: 8px;\n" +
                    "                    text-decoration: none;\n" +
                    "                }\n" +
                    "                #sct2 table tr th, #sct2 table tr td b{\n" +
                    "                    font-weight: bold;\n" +
                    "                }\n" +
                    "                #sct2 table tr:nth-child(even) {\n" +
                    "                    background-color: #dddddd;\n" +
                    "                }\n" +
                    "                .vacia {\n" +
                    "                    border: 0 none;\n" +
                    "                }\n" +
                    "            </style>\n" +
                    "        </head>\n" +
                    "        <body>\n" +
                    "            <main>\n" +
                    "                <section id=\"sct1\">\n" +
                    "                    <div>\n" +
                    "                        <img src=\"cid:image\"/>\n" +
                    "                        <h1>\n" +
                    "                            Aeropuerto Internacional de Orotina\n" +
                    "                        </h1>\n" +
                    "                        <p>\n" +
                    "                            <b>Factura de su compra para el vuelo " + v[0] + "</b>\n" +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Origen: \n" + origen +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Gate: \n" + s[1] +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Fecha de salida: \n" + v[13] +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Hora de salida: \n" + v[3] +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Destino: \n" + destino +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Fecha de llegada: \n" + v[12] +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Hora de llegada: \n" + v[5] +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Ubicación: Orotina, Alajuela, Costa Rica\n" +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Tel.: +(506) 8939-7940\n" +
                    "                        </p>\n" +
                    "                        <p>\n" +
                    "                            Correo: info@aeropuertoorotina.com\n" +
                    "                        </p>\n" +
                    "                    </div>\n" +
                    "                </section>\n" +
                    "                <section id=\"sct2\">\n" +
                    "                    <h2>\n" +
                    "                        Detalle del tiquete\n" +
                    "                    </h2>\n" +
                    "                    <table id=\"tblLibrosReservados\" class=\"ocultar\">" +
                    "<thead>" +
                    "<tr>" +
                    "<th>Vuelo</th>" +
                    "<th>Asiento</th>" +
                    "<th>Tipo de asiento</th>" +
                    "<th>Fecha de compra</th>" +
                    "<th>Precio</th>" +
                    "<th>Impuesto</th>" +
                    "<th>Total</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>" +
                    "<tr>" +
                    "<td>" + v[0] + "</td>" +
                    "<td>" + tmpTiquete.getNumeroAsiento() + "</td>" +
                    "<td>" + tipoAsiento + "</td>" +
                    "<td>" + LocalDate.now() + "</td>" +
                    "<td>" + v[14] + "</td>" +
                    "<td>" + impuesto + "</td>" +
                    "<td>" + (Double.parseDouble(v[14]) + impuesto) + "</td>" +
                    "</tr>" +
                    "</tbody>" +
                    "</table>" +
                    "            </main>\n" +
                    "        </body>\n" +
                    "    </html>";

            try {
                Session session = Session.getDefaultInstance(props, new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

                Message msg = new MimeMessage(session);

                msg.setFrom(new InternetAddress("info@aeropuertoorotina.com"));
                msg.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(personaIngresada.getCorreo(), false));
                msg.setSubject("Factura de su compra");

                MimeBodyPart p1 = new MimeBodyPart();
                p1.setDataHandler(new DataHandler(new HTMLDataSource(EMAIL_TEXT)));

                MimeBodyPart p2 = new MimeBodyPart();

                FileDataSource fds = new FileDataSource("C:\\Users\\miran\\Documents\\IntellijIDEAProjects\\AeropuertoLib\\logo.png");
                p2.setDataHandler(new DataHandler(fds));
                p2.setFileName(fds.getName());
                p2.setHeader("Content-ID", "<image>");

                Multipart mp = new MimeMultipart();
                mp.addBodyPart(p1);
                mp.addBodyPart(p2);

                msg.setContent(mp);

                msg.setSentDate(new Date());

                Transport.send(msg);
            } catch (MessagingException e) {
                System.out.println("Error de envío, causa: " + e);
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

}
