package castaing.miranda.excepciones;

import castaing.miranda.utils.Messages;

public class AeropuertoException extends Exception {

    private int numero;

    public AeropuertoException() {
        super();
    }

    public AeropuertoException(String message) {
        super(message);
    }

    public AeropuertoException(int numero) {
        this.numero = numero;
    }

    public String numeroToString() {
        switch (numero) {
            case 2627:
                return Messages.MNSJ_REG_REPETIDO;
            default:
                return Messages.MNSJ_NO_DEFINIDO;
        }
    }

}
