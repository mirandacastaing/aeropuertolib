package castaing.miranda.tl;

import castaing.miranda.bl.dao.administrador.Administrador;
import castaing.miranda.bl.dao.administrador.IAdministradorDao;
import castaing.miranda.bl.dao.aerea.linea.ILineaAereaDao;
import castaing.miranda.bl.dao.aerea.linea.LineaAerea;
import castaing.miranda.bl.dao.aeronave.Aeronave;
import castaing.miranda.bl.dao.aeronave.IAeronaveDao;
import castaing.miranda.bl.dao.aeropuerto.Aeropuerto;
import castaing.miranda.bl.dao.aeropuerto.IAeropuertoDao;
import castaing.miranda.bl.dao.factory.DaoFactory;
import castaing.miranda.bl.dao.pais.IPaisDao;
import castaing.miranda.bl.dao.pais.Pais;
import castaing.miranda.bl.dao.sala.ISalaDao;
import castaing.miranda.bl.dao.sala.Sala;
import castaing.miranda.bl.dao.tiquete.Tiquete;
import castaing.miranda.bl.dao.tripulacion.ITripulacionDao;
import castaing.miranda.bl.dao.tripulacion.Tripulacion;
import castaing.miranda.bl.dao.tripulante.IMiembroTripulacionDao;
import castaing.miranda.bl.dao.tripulante.MiembroTripulacion;
import castaing.miranda.bl.dao.ubicacion.IUbicacionDao;
import castaing.miranda.bl.dao.ubicacion.Ubicacion;
import castaing.miranda.bl.dao.usuario.IUsuarioDao;
import castaing.miranda.bl.dao.usuario.Usuario;
import castaing.miranda.bl.dao.vuelo.IVueloDao;
import castaing.miranda.bl.dao.vuelo.Vuelo;
import castaing.miranda.dl.CapaLogica;
import castaing.miranda.excepciones.AeropuertoException;

import java.sql.SQLException;
import java.util.ArrayList;

public class Controller {

    CapaLogica logica = new CapaLogica();

    public Controller() {

    }

    public boolean verificarVuelo(String numeroVuelo) throws AeropuertoException {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        try {
            if (daoVuelo.buscarVueloPorNumero(numeroVuelo)[0] != null) {
                existe = true;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public boolean verificarAeronave(String placa) throws AeropuertoException {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronaveDao daoAeronave = factory.getAeronaveDao();
        try {
            if (daoAeronave.buscarAeronavePorPlaca(placa)[0] != null) {
                existe = true;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public boolean verificarLineaAerea(String cedula, String nombreComercial) throws AeropuertoException {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory.getLineaAereaDao();
        try {
            if (cedula != null) {
                if (daoLineaAerea.buscarLineaAereaPorCedula(cedula)[0] != null) {
                    existe = true;
                } else if (daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercial)[0] != null) {
                    existe = true;
                }
            } else {
                if (daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercial)[0] != null) {
                    existe = true;
                }
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public boolean verificarSala(String codigo, String nombre) throws AeropuertoException {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory.getSalaDao();
        try {
            if (codigo != null) {
                if (daoSala.buscarSalaPorCodigo(codigo)[0] != null) {
                    existe = true;
                } else if (daoSala.buscarSalaPorNombre(nombre)[0] != null) {
                    existe = true;
                }
            } else {
                if (daoSala.buscarSalaPorNombre(nombre)[0] != null) {
                    existe = true;
                }
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public boolean verificarUbicacion(char planta) throws Exception {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUbicacionDao daoUbicacion = factory.getUbicacionDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuerto = factory2.getAeropuertoDao();
        try {
            String[] aeropuerto = daoAeropuerto.getAeropuertoDeAdministrador(logica.getPersonaIngresada().getCorreo());
            if (daoUbicacion.buscarUbicacionPorPlantaAeropuerto(planta, aeropuerto[1]).getPlanta() != ' ') {
                    existe = true;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public boolean verificarPais(String codigo, String nombre) throws AeropuertoException {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        try {
            if (codigo != null) {
                if (!daoPais.buscarPaisPorCodigo(codigo).getCodigo().equals(" ")) {
                    existe = true;
                } else if (!daoPais.buscarPaisPorNombre(nombre).getCodigo().equals(" ")) {
                    existe = true;
                }
            } else {
                if (!daoPais.buscarPaisPorNombre(nombre).getCodigo().equals(" ")) {
                    existe = true;
                }
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public boolean verificarTripulacion(String codigo, String nombre, String aerolinea) throws AeropuertoException {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory.getTripulacionDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        try {
            String[] info = daoLineaAerea.buscarLineaAereaPorNombreComercial(aerolinea);
            if (codigo != null) {
                if (daoTripulacion.buscarTripulacionPorCodigo(codigo)[0] != null) {
                    existe = true;
                } else if (daoTripulacion.buscarTripulacionPorNombre(nombre, info[0])[0] != null) {
                    existe = true;
                }
            } else {
                if (daoTripulacion.buscarTripulacionPorNombre(nombre, info[0])[0] != null) {
                    existe = true;
                }
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public boolean verificarUsuario(String correo, String identificacion) throws AeropuertoException {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAdministradorDao daoAdministrador = factory.getAdministradorDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IMiembroTripulacionDao daoMiembroTripulacion = factory2.getMiembroTripulacionDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUsuarioDao daoUsuario = factory3.getUsuarioDao();
        try {
            if (daoUsuario.validarUsuario(correo, identificacion)) {
                existe = true;
            } else if (daoMiembroTripulacion.validarMiembroTripulacion(correo, identificacion)) {
                existe = true;
            } else if (daoAdministrador.validarAdministrador(correo, identificacion)) {
                existe = true;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public boolean verificarAdministradorAeropuertoRegistrados() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoObject = factory.getAeropuertoDao();
        try {
            return daoObject.validarAeropuertoRegistrado();
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String verificarCredenciales(String correo, String contrasenna) throws AeropuertoException {
        String fxml = " ";
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAdministradorDao daoAdministrador = factory.getAdministradorDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IMiembroTripulacionDao daoMiembroTripulacion = factory2.getMiembroTripulacionDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUsuarioDao daoUsuario = factory3.getUsuarioDao();
        DaoFactory factory4 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuerto = factory4.getAeropuertoDao();
        try {
            Administrador a = daoAdministrador.validarCredenciales(correo, contrasenna);
            MiembroTripulacion m = daoMiembroTripulacion.validarCredenciales(correo, contrasenna);
            String[] u = daoUsuario.validarCredenciales(correo, contrasenna);
            if (u != null) {
                Usuario us = new Usuario(u[0], u[1], u[2], u[3], u[4], u[5], u[6], u[7], u[8], u[9], u[10]);
                System.out.println("4");
                logica.iniciarSesion(us);
                fxml = "Llegadas.fxml";
            } else if (m != null) {
                logica.iniciarSesion(m);
                fxml = "Programados.fxml";
            } else if (a != null) {
                logica.iniciarSesion(a);
                if (daoAeropuerto.validarAdministradorAsignadoAAeropuerto(correo)) {
                    fxml = "ListarVuelos.fxml";
                } else {
                    fxml = "administradorSinAeropuerto";
                }
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return fxml;
    }

    public String obtenerNombreCompletoPersonaIngresada() {
        return logica.getPersonaIngresada().getNombre() + " " + logica.getPersonaIngresada().getApellidos();
    }

    public void procesarUsuario(String nombre, String apellidos, String identificacion, String correo, String direccion,
                                String nacionalidad, String provincia, String canton, String distrito, String
                                        fechaNacimiento) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUsuarioDao daoObject = factory.getUsuarioDao();
        String contrasenna = logica.generarContrasenna();
        try {
            daoObject.insertar(nombre, apellidos, identificacion, correo, direccion, nacionalidad, contrasenna,
                    provincia, canton, distrito, fechaNacimiento);
            logica.enviarContrasenna(correo, contrasenna);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarUsuario(String nombre, String apellidos, String identificacion, String correo,
                                  String direccion, String nacionalidad, String contrasenna, String provincia,
                                  String canton, String distrito, String fechaNacimiento) {
        Usuario tmpUsuario;
        tmpUsuario = new Usuario(nombre, apellidos, identificacion, correo, direccion, nacionalidad, contrasenna,
                provincia, canton, distrito, fechaNacimiento);
        logica.modificarPersona(tmpUsuario);
    }

    public void procesarAdministrador(String nombre, String apellidos, String identificacion, String correo,
                                      String direccion, String nacionalidad, String fechaNacimiento, String genero)
            throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAdministradorDao daoObject = factory.getAdministradorDao();
        String contrasenna = logica.generarContrasenna();
        try {
            daoObject.insertar(nombre, apellidos, identificacion, correo, direccion, nacionalidad, contrasenna,
                    fechaNacimiento, genero);
            logica.enviarContrasenna(correo, contrasenna);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarAdministrador(String nombre, String apellidos, String identificacion, String correo,
                                        String direccion, String nacionalidad, String contrasenna,
                                        String fechaNacimiento, String genero) {
        Administrador tmpAdministrador;
        tmpAdministrador = new Administrador(nombre, apellidos, identificacion, correo, direccion, nacionalidad,
                contrasenna, fechaNacimiento, genero);
        logica.modificarPersona(tmpAdministrador);
    }

    public void procesarAeropuertoPais(String nombreAeropuerto, String codigoAeropuerto, String codigoPais,
                                       String nombrePais, String abreviaturaPais, String correoAdministrador) throws
            AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        IAdministradorDao daoAdministrador = factory.getAdministradorDao();
        IAeropuertoDao daoAeropuerto = factory.getAeropuertoDao();
        try {
            daoPais.insertar(codigoPais, nombrePais, abreviaturaPais);
            daoAeropuerto.insertar(nombreAeropuerto, codigoAeropuerto, codigoPais,
                    daoAdministrador.buscarAdministradorPorCorreo(correoAdministrador).getIdentificacion());
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarAeropuerto(String nombreAeropuerto, String codigoAeropuerto, String nombrePais,
                                   String correoAdministrador) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAdministradorDao daoAdministrador = factory2.getAdministradorDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuerto = factory3.getAeropuertoDao();
        try {
            String administrador = daoAdministrador.buscarAdministradorPorCorreo(correoAdministrador).getIdentificacion();
            daoAeropuerto.insertar(nombreAeropuerto, codigoAeropuerto,
                    daoPais.buscarPaisPorNombre(nombrePais).getCodigo(), administrador);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarAeropuerto(String nombreAeropuerto, String codigoAeropuerto, String codigoPais,
                                     String correoAdministrador) throws AeropuertoException {
        Aeropuerto tmpAeropuerto;
        try {
            tmpAeropuerto = new Aeropuerto(nombreAeropuerto, codigoAeropuerto, logica.buscarPaisPorCodigo(codigoPais),
                    ((Administrador) logica.buscarPersonaPorCorreo(correoAdministrador)));
            logica.modificarAeropuerto(tmpAeropuerto);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void borrarAeropuerto(String codigoAeropuerto) {
        Aeropuerto tmpAeropuerto = new Aeropuerto();
        tmpAeropuerto.setCodigo(codigoAeropuerto);
        logica.eliminarAeropuerto(tmpAeropuerto);
    }

    public void procesarUbicacion(char planta, String descripcion) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUbicacionDao daoUbicacion = factory.getUbicacionDao();
        IAeropuertoDao daoObject = factory.getAeropuertoDao();
        IAdministradorDao daoAdministrador = factory.getAdministradorDao();
        IPaisDao daoPais = factory.getPaisDao();
        try {
            String[] aeropuerto = daoObject.getAeropuertoDeAdministrador(logica.getPersonaIngresada().getCorreo());
            daoUbicacion.insertar(planta, descripcion, aeropuerto[1]);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarUbicacion(char planta, String descripcion) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUbicacionDao daoObject = factory.getUbicacionDao();
        try {
            daoObject.modificar(planta, descripcion, logica.getPersonaIngresada().getCorreo());
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void borrarUbicacion(char planta) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUbicacionDao daoObject = factory.getUbicacionDao();
        try {
            daoObject.eliminar(planta, logica.getPersonaIngresada().getCorreo());
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarSala(String nombre, String codigo, char planta) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoObject = factory.getSalaDao();
        try {
            daoObject.insertar(nombre, codigo, logica.getPersonaIngresada().getCorreo(), planta);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarSala(String nombre, String codigo, char planta) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoObject = factory.getSalaDao();
        try {
            daoObject.modificar(nombre, codigo, planta);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void borrarSala(String codigo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoObject = factory.getSalaDao();
        try {
            daoObject.eliminar(codigo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarPais(String codigo, String nombre, String abreviatura) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        try {
            daoPais.insertar(codigo, nombre, abreviatura);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarPais(String codigo, String nombre, String abreviatura) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        try {
            daoPais.modificar(codigo, nombre, abreviatura);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void borrarPais(String codigo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IPaisDao daoPais = factory.getPaisDao();
        try {
            daoPais.eliminar(codigo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarLineaAerea(String nombreComercial, String cedula, String nombreEmpresa, String nombrePais,
                                   String logo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory.getLineaAereaDao();
        try {
            daoLineaAerea.insertar(nombreComercial, cedula, nombreEmpresa, nombrePais, logo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarLineaAerea(String nombreComercial, String cedula, String nombreEmpresa, String nombrePais,
                                     String logo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory.getLineaAereaDao();
        try {
            daoLineaAerea.modificar(nombreComercial, cedula, nombreEmpresa, nombrePais, logo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void borrarLineaAerea(String cedula) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory.getLineaAereaDao();
        try {
            daoLineaAerea.eliminar(cedula);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarVuelo(String nombreComercialLineaAerea, String nombreAeropuertoOrigen, String horaLlegada,
                              String horaSalida, String nombreAeropuertoDestino, char tipo, String placaAeronave,
                              String nombreSala, String nombreTripulacion, String numeroVuelo, String fechaLlegada,
                              String fechaSalida, double precio) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO = factory2.getAeropuertoDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory3.getSalaDao();
        DaoFactory factory4 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD = factory4.getAeropuertoDao();
        DaoFactory factory5 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripuacion = factory5.getTripulacionDao();
        DaoFactory factory6 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronaveDao daoAeronave = factory6.getAeronaveDao();
        DaoFactory factory7 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory7.getLineaAereaDao();
        char estado;
        if (tipo == 'L') {
            estado = logica.asignarEstadoCorrecto(fechaLlegada + " " + horaLlegada);
        } else {
            estado = logica.asignarEstadoCorrecto(fechaSalida + " " + horaSalida);
        }
        try {
            String lineaAerea = daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercialLineaAerea)[0];
            daoVuelo.insertar(daoAeropuertoO.buscarAeropuertoPorNombre(nombreAeropuertoOrigen)[0], horaLlegada,
                    horaSalida, estado, daoAeropuertoD.buscarAeropuertoPorNombre(nombreAeropuertoDestino)[0], tipo,
                    placaAeronave, daoSala.buscarSalaPorNombre(nombreSala)[0],
                    daoTripuacion.buscarTripulacionPorNombre(nombreTripulacion, lineaAerea)[0], numeroVuelo,
                    Integer.parseInt(daoAeronave.buscarAeronavePorPlaca(placaAeronave)[3]), fechaLlegada, fechaSalida,
                    precio, lineaAerea);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarVuelo(String nombreComercialLineaAerea, String nombreAeropuertoOrigen, String horaLlegada,
                                String horaSalida, String nombreAeropuertoDestino, char tipo, String placaAeronave,
                                String nombreSala, String nombreTripulacion, String numeroVuelo, char estado,
                                String fechaLlegada, String fechaSalida, double precio) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoO = factory2.getAeropuertoDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ISalaDao daoSala = factory3.getSalaDao();
        DaoFactory factory4 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuertoD = factory4.getAeropuertoDao();
        DaoFactory factory5 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripuacion = factory5.getTripulacionDao();
        DaoFactory factory6 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronaveDao daoAeronave = factory6.getAeronaveDao();
        DaoFactory factory7 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory7.getLineaAereaDao();
        try {
            String lineaAerea = daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercialLineaAerea)[0];
            daoVuelo.modificar(daoAeropuertoO.buscarAeropuertoPorNombre(nombreAeropuertoOrigen)[0], horaLlegada,
                    horaSalida, estado, daoAeropuertoD.buscarAeropuertoPorNombre(nombreAeropuertoDestino)[0], tipo,
                    placaAeronave, daoSala.buscarSalaPorNombre(nombreSala)[0],
                    daoTripuacion.buscarTripulacionPorNombre(nombreTripulacion, lineaAerea)[0], numeroVuelo,
                    fechaLlegada, fechaSalida, precio, lineaAerea);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public boolean validarUsuario(String correo, String identificacion) throws AeropuertoException {
        boolean existe = false;
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IUsuarioDao daoUsuario = factory.getUsuarioDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IMiembroTripulacionDao daoMiembroTripulacion = factory2.getMiembroTripulacionDao();
        DaoFactory factory3 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAdministradorDao daoAdministrador = factory3.getAdministradorDao();
        try {
            if (daoUsuario.validarUsuario(correo, identificacion)) {
                existe = true;
            } else if (daoMiembroTripulacion.validarMiembroTripulacion(correo, identificacion)) {
                existe = true;
            } else if (daoAdministrador.validarAdministrador(correo, identificacion)) {
                existe = true;
            }
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
        return existe;
    }

    public void borrarVuelo(String numeroVuelo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IVueloDao daoVuelo = factory.getVueloDao();
        try {
            daoVuelo.eliminar(numeroVuelo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarTripulacion(String nombreComercialLineaAerea, String codigo, String nombre) throws
            AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory.getTripulacionDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        try {
            daoTripulacion.insertar(codigo, nombre,
                    daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercialLineaAerea)[0]);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarTripulacion(String nombreComercialLineaAerea, String codigo, String nombre) throws
            AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory.getTripulacionDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        try {
            daoTripulacion.modificar(codigo, nombre,
                    daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercialLineaAerea)[0]);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void borrarTripulacion(String codigo) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ITripulacionDao daoTripulacion = factory.getTripulacionDao();
        try {
            daoTripulacion.eliminar(codigo);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarMiembroTripulacion(String nombre, String apellidos, String identificacion, String correo,
                                           String direccion, String nacionalidad, char genero, int annosExperiencia,
                                           String fechaGraduacion, String numeroLicencia, char puesto, String telefono,
                                           String nombreComercialLineaAerea, String nombreTripulacion) throws
            AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IMiembroTripulacionDao daoMiembroTripulacion = factory.getMiembroTripulacionDao();
        String contrasenna = logica.generarContrasenna();
        try {
            daoMiembroTripulacion.insertar(nombre, apellidos, identificacion, correo, direccion, nacionalidad,
                    contrasenna, genero, annosExperiencia, fechaGraduacion, numeroLicencia, puesto, telefono,
                    nombreComercialLineaAerea, nombreTripulacion);
            logica.enviarContrasenna(correo, contrasenna);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarMiembroTripulacion(String nombre, String apellidos, String identificacion, String correo,
                                             String direccion, String nacionalidad, String contrasenna, char genero,
                                             int annosExperiencia, String fechaGraduacion, String numeroLicencia,
                                             char puesto, String telefono) {
        MiembroTripulacion tmpMiembroTripulacion;
        tmpMiembroTripulacion = new MiembroTripulacion(nombre, apellidos, identificacion, correo, direccion,
                nacionalidad, contrasenna, genero, annosExperiencia, fechaGraduacion, numeroLicencia, puesto, telefono);
        logica.modificarPersona(tmpMiembroTripulacion);
    }

    public void procesarAeronave(String nombreComercialLineaAerea, String placa, String marca, String modelo,
                                 int capacidad) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronaveDao daoAeronave = factory.getAeronaveDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        try {
            daoAeronave.insertar(placa, marca, modelo, capacidad,
                    daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercialLineaAerea)[0]);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void actualizarAeronave(String nombreComercialLineaAerea, String placa, String marca, String modelo, int
            capacidad) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronaveDao daoAeronave = factory.getAeronaveDao();
        DaoFactory factory2 = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        ILineaAereaDao daoLineaAerea = factory2.getLineaAereaDao();
        try {
            daoAeronave.modificar(placa, marca, modelo, capacidad,
                    daoLineaAerea.buscarLineaAereaPorNombreComercial(nombreComercialLineaAerea)[0]);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void borrarAeronave(String placa) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeronaveDao daoAeronave = factory.getAeronaveDao();
        try {
            daoAeronave.eliminar(placa);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarTiquete(String numeroAsiento, double precio, String numeroVuelo, char tipoAsiento) throws
            AeropuertoException {
        try {
            logica.registrarTiquete(numeroAsiento, precio, numeroVuelo, tipoAsiento);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public void procesarImpuesto(double impuesto) throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuerto = factory.getAeropuertoDao();
        try {
            daoAeropuerto.modificarImpuesto(impuesto, daoAeropuerto.getAeropuertoDeAdministrador(logica.getPersonaIngresada().getCorreo())[1]);
        } catch (SQLException e) {
            int error = e.getErrorCode();
            AeropuertoException exc = new AeropuertoException(error);
            throw exc;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarUbicaciones() throws AeropuertoException {
        try {
            return logica.getUbicaciones();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarSalas() throws AeropuertoException {
        try {
            return logica.getSalas();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarPaises() throws AeropuertoException {
        try {
            return logica.getPaises();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarLineasAereas() throws AeropuertoException {
        try {
            return logica.getLineasAereas();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarVuelos() throws AeropuertoException {
        try {
            return logica.getVuelos();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarVuelosLlegada() throws AeropuertoException {
        try {
            return logica.getVuelosLlegada();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarVuelosSalida() throws AeropuertoException {
        try {
            return logica.getVuelosSalida();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarTripulaciones() throws AeropuertoException {
        try {
            return logica.getTripulaciones();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarAeronaves() throws AeropuertoException {
        try {
            return logica.getAeronaves();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarUsuarios() {
        return logica.getUsuarios();
    }

    public String[][] listarAeropuertos() throws AeropuertoException {
        try {
            return logica.getAeropuertos();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[][] listarVuelosMiembroTripulacion() {
        return logica.getVuelosMiembroTripulacion();
    }

    public String[] getSala(String codigo) throws AeropuertoException {
        try {
            return logica.buscarSalaPorCodigo(codigo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[] getUbicacion(char planta) throws AeropuertoException {
        String[] info = new String[2];
        try {
            Ubicacion tmpUbicacion = logica.buscarUbicacionPorPlanta(planta);
            info[0] = Character.toString(tmpUbicacion.getPlanta());
            info[1] = tmpUbicacion.getDescripcion();
            return info;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[] getPais(String codigo) throws AeropuertoException {
        String[] info = new String[3];
        try {
            Pais tmpPais = logica.buscarPaisPorCodigo(codigo);
            info[0] = tmpPais.getCodigo();
            info[1] = tmpPais.getNombre();
            info[2] = tmpPais.getAbreviatura();
            return info;
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[] getLineaAerea(String cedula) throws AeropuertoException {
        try {
            return logica.buscarLineaAereaPorCedula(cedula);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[] getAeronave(String placa) throws AeropuertoException {
        try {
            return ((String[]) logica.buscarAeronavePorPlaca(placa)[1]);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[] getTripulacion(String codigo) throws AeropuertoException {
        try {
            return ((String[]) logica.buscarTripulacionPorCodigo(codigo)[1]);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String[] getVuelo(String numero) throws AeropuertoException {
        try {
            return ((String[]) logica.buscarVueloPorNumero(numero)[1]);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

    public String getImpuestoAeropuerto() throws AeropuertoException {
        DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
        IAeropuertoDao daoAeropuerto = factory.getAeropuertoDao();
        try {
            return Double.toString(daoAeropuerto.getImpuesto(daoAeropuerto.getAeropuertoDeAdministrador(logica.getPersonaIngresada().getCorreo())[1]));
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            throw exc;
        }
    }

}
