package castaing.miranda.utils;

public class Messages {

    public static final String MNSJ_EXITO = "Operación ejecutada exitosamente";
    public static final String MNSJ_REG_REPETIDO = "El registro ya está";
    public static final String MNSJ_NO_DEFINIDO = "Error indefinido";

}
